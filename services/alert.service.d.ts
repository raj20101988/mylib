import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class AlertService {
    private _http;
    constructor(_http: HttpClient);
    getAlertsData(url: any, options: {}): Observable<HttpResponse<Object>>;
    getEventData(): void;
    getAlertCounts(): void;
    changeStatus(statusUrl: any, statusObj: any): Observable<HttpResponse<Object>>;
    changeSeverity(): void;
    addNote(noteUrl: any, noteObj: any): Observable<HttpResponse<Object>>;
    saveCustomerKnowledgeBase(baseUrl: any, baseObj: any): Observable<HttpResponse<Object>>;
}
