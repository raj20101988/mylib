import { JwtHelperService } from '@auth0/angular-jwt';
export declare class DecodedTokenService {
    private jwtHelper;
    constructor(jwtHelper: JwtHelperService);
    private decodedJwtToken;
    getDecodedJwtToken(): any;
    setDecodedJwtToken(token: any): void;
}
