import { JwtHelperService } from '@auth0/angular-jwt';
export declare class TokenHandlingService {
    private jwtHelper;
    authValues: any;
    constructor(jwtHelper: JwtHelperService);
    redirectURI(): void;
    isTokenValid(jwtToken: any): boolean;
    hasPermission(jwtToken: any): boolean;
}
