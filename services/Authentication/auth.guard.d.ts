import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenHandlingService } from './token-handling.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { DecodedTokenService } from './decoded-token.service';
export declare class AuthGuard implements CanActivate {
    private tokenHandlingService;
    private jwtHelper;
    private decodedTokenService;
    jwtToken: string;
    isTokenValid: any;
    constructor(tokenHandlingService: TokenHandlingService, jwtHelper: JwtHelperService, decodedTokenService: DecodedTokenService);
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean;
}
