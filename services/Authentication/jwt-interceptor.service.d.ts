import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
/**
 * This service adds jwt token on every API call
 */
export declare class JwtInterceptorService implements HttpInterceptor {
    private jwtHelper;
    constructor(jwtHelper: JwtHelperService);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
