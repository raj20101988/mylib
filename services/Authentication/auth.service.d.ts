import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class AuthService {
    private http;
    constructor(http: HttpClient);
    getRefreshToken(clientId: any): Observable<any>;
}
