import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
export declare class TopNavBarComponent implements OnInit {
    private _router;
    translate: TranslateService;
    lastRefresh: string;
    constructor(_router: Router, translate: TranslateService);
    ngOnInit(): void;
    getLastRefresh(): void;
    toggleTranslation(): void;
    refresh(): void;
}
