/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { TopologyTreeComponent } from '../topology-tree/topology-tree.component';
import { TopologytreeService } from '../services/topologytree.service';
import { MaterialComponentModule } from '../material-component/material-component.module';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
/**
 * this module provide the topology tree component, service etc.. as you will add later like Pipe, Filters etc...
 * exported into the CommonUILib Module.
 */
export class TopologyTreeModule {
}
TopologyTreeModule.decorators = [
    { type: NgModule, args: [{
                imports: [MaterialComponentModule, CommonModule, DragDropModule],
                declarations: [TopologyTreeComponent],
                exports: [TopologyTreeComponent],
                providers: [TopologytreeService],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9wb2xvZ3ktdHJlZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJtb2R1bGVzL3RvcG9sb2d5LXRyZWUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLDBDQUEwQyxDQUFDO0FBQy9FLE9BQU8sRUFBQyxtQkFBbUIsRUFBQyxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBQyx1QkFBdUIsRUFBQyxNQUFNLGlEQUFpRCxDQUFDO0FBQ3hGLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sd0JBQXdCLENBQUM7Ozs7O0FBWXRELE1BQU0sT0FBTyxrQkFBa0I7OztZQU45QixRQUFRLFNBQUM7Z0JBQ04sT0FBTyxFQUFFLENBQUMsdUJBQXVCLEVBQUUsWUFBWSxFQUFFLGNBQWMsQ0FBQztnQkFDaEUsWUFBWSxFQUFFLENBQUMscUJBQXFCLENBQUM7Z0JBQ3JDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO2dCQUNoQyxTQUFTLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQzthQUNuQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1RvcG9sb2d5VHJlZUNvbXBvbmVudH0gZnJvbSAnLi4vdG9wb2xvZ3ktdHJlZS90b3BvbG9neS10cmVlLmNvbXBvbmVudCc7XG5pbXBvcnQge1RvcG9sb2d5dHJlZVNlcnZpY2V9IGZyb20gJy4uL3NlcnZpY2VzL3RvcG9sb2d5dHJlZS5zZXJ2aWNlJztcbmltcG9ydCB7TWF0ZXJpYWxDb21wb25lbnRNb2R1bGV9IGZyb20gJy4uL21hdGVyaWFsLWNvbXBvbmVudC9tYXRlcmlhbC1jb21wb25lbnQubW9kdWxlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtEcmFnRHJvcE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY2RrL2RyYWctZHJvcCc7XG5cbi8qKlxuICogdGhpcyBtb2R1bGUgcHJvdmlkZSB0aGUgdG9wb2xvZ3kgdHJlZSBjb21wb25lbnQsIHNlcnZpY2UgZXRjLi4gYXMgeW91IHdpbGwgYWRkIGxhdGVyIGxpa2UgUGlwZSwgRmlsdGVycyBldGMuLi5cbiAqIGV4cG9ydGVkIGludG8gdGhlIENvbW1vblVJTGliIE1vZHVsZS5cbiAqL1xuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbTWF0ZXJpYWxDb21wb25lbnRNb2R1bGUsIENvbW1vbk1vZHVsZSwgRHJhZ0Ryb3BNb2R1bGVdLFxuICAgIGRlY2xhcmF0aW9uczogW1RvcG9sb2d5VHJlZUNvbXBvbmVudF0sXG4gICAgZXhwb3J0czogW1RvcG9sb2d5VHJlZUNvbXBvbmVudF0sXG4gICAgcHJvdmlkZXJzOiBbVG9wb2xvZ3l0cmVlU2VydmljZV0sXG59KVxuZXhwb3J0IGNsYXNzIFRvcG9sb2d5VHJlZU1vZHVsZSB7IH1cbiJdfQ==