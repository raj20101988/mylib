/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class SortingService {
    constructor() { }
    /**
     * @param {?} sort
     * @return {?}
     */
    getSortObject(sort) {
        this.sortObj = { sortOrder: sort.direction.toUpperCase(), sortField: sort.active };
        return this.sortObj;
    }
}
SortingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SortingService.ctorParameters = () => [];
/** @nocollapse */ SortingService.ngInjectableDef = i0.defineInjectable({ factory: function SortingService_Factory() { return new SortingService(); }, token: SortingService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SortingService.prototype.sortObj;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ydGluZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsic29ydGluZy9zb3J0aW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzNDLE1BQU0sT0FBTyxjQUFjO0lBRXpCLGdCQUFnQixDQUFDOzs7OztJQUNqQixhQUFhLENBQUMsSUFBSTtRQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNsRixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDdEIsQ0FBQzs7O1lBVEYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7O0lBRUcsaUNBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBTb3J0aW5nU2VydmljZSB7XG4gICAgc29ydE9iajogb2JqZWN0O1xuICBjb25zdHJ1Y3RvcigpIHsgfVxuICBnZXRTb3J0T2JqZWN0KHNvcnQpOiBvYmplY3Qge1xuICAgIHRoaXMuc29ydE9iaiA9IHtzb3J0T3JkZXI6IHNvcnQuZGlyZWN0aW9uLnRvVXBwZXJDYXNlKCksIHNvcnRGaWVsZDogc29ydC5hY3RpdmUgfTtcbiAgICByZXR1cm4gdGhpcy5zb3J0T2JqO1xuICB9XG59XG4iXX0=