/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
export class LoaderComponent {
    constructor() {
    }
    /**
     * @param {?} count
     * @return {?}
     */
    generateFake(count) {
        /** @type {?} */
        const indexes = [];
        for (let i = 0; i < count; i++) {
            indexes.push(i);
        }
        return indexes;
    }
}
LoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-loader',
                template: "<div class=\"skeleton\">\n    <div *ngFor=\"let fake of generateFake(4)\" class=\"card-layout\">\n        <h3></h3>\n        <h4></h4>\n        <p></p>\n    </div>\n</div>\n",
                styles: [".skeleton{padding:0 10%}.skeleton h3,.skeleton h4,.skeleton p{-webkit-animation:1.7s linear infinite loading;animation:1.7s linear infinite loading;background:no-repeat #f6f7f8;background-image:linear-gradient(to left,#f6f7f8 0,#edeef1 20%,#f6f7f8 40%,#f6f7f8 100%);width:100%;margin-bottom:5px}.skeleton h3{height:22px;width:40%}.skeleton h4{height:18px;width:65%}.skeleton p{height:18px;margin-bottom:50px}@-webkit-keyframes loading{0%{background-position:-100px}100%{background-position:200px}}@keyframes loading{0%{background-position:-100px}100%{background-position:200px}}"]
            }] }
];
/** @nocollapse */
LoaderComponent.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbImxvYWRlci9sb2FkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBT3hDLE1BQU0sT0FBTyxlQUFlO0lBQ3hCO0lBQ0EsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsS0FBYTs7Y0FDaEIsT0FBTyxHQUFHLEVBQUU7UUFDbEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ25CO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7O1lBZkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxZQUFZO2dCQUN0Qix5TEFBc0M7O2FBRXpDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FwcC1sb2FkZXInLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9sb2FkZXIuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2xvYWRlci5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIExvYWRlckNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgfVxuXG4gICAgZ2VuZXJhdGVGYWtlKGNvdW50OiBudW1iZXIpOiBBcnJheTxudW1iZXI+IHtcbiAgICAgICAgY29uc3QgaW5kZXhlcyA9IFtdO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNvdW50OyBpKyspIHtcbiAgICAgICAgICAgIGluZGV4ZXMucHVzaChpKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaW5kZXhlcztcbiAgICB9XG59XG4iXX0=