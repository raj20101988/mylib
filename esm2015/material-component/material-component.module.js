/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { MatButtonModule, MatButtonToggleModule, MatCardModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatRadioModule, MatSelectModule, MatTableModule, MatMenuModule, MatTabsModule, MatExpansionModule, MatBottomSheetModule, MatCheckboxModule, MatSnackBarModule, MatTooltipModule, MatDialogModule, MatDatepickerModule, MatNativeDateModule, MatToolbarModule, MatTreeModule, MatSidenavModule, MatDividerModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
export class MaterialComponentModule {
}
MaterialComponentModule.decorators = [
    { type: NgModule, args: [{
                exports: [
                    MatButtonModule,
                    MatCardModule,
                    MatGridListModule,
                    MatInputModule,
                    MatSelectModule,
                    MatFormFieldModule,
                    MatRadioModule,
                    MatListModule,
                    MatIconModule,
                    MatButtonToggleModule,
                    MatTableModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatMenuModule,
                    MatTabsModule,
                    MatExpansionModule,
                    MatBottomSheetModule,
                    MatCheckboxModule,
                    MatSnackBarModule,
                    MatTooltipModule,
                    MatDialogModule,
                    MatDatepickerModule,
                    MatNativeDateModule,
                    MatIconModule,
                    MatToolbarModule,
                    MatTreeModule,
                    MatSidenavModule,
                    MatDividerModule
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtY29tcG9uZW50Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbIm1hdGVyaWFsLWNvbXBvbmVudC9tYXRlcmlhbC1jb21wb25lbnQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFDTCxlQUFlLEVBQUUscUJBQXFCLEVBQ3RDLGFBQWEsRUFDYixrQkFBa0IsRUFDbEIsaUJBQWlCLEVBQUUsYUFBYSxFQUNoQyxjQUFjLEVBQUUsYUFBYSxFQUM3QixjQUFjLEVBQ2QsZUFBZSxFQUFFLGNBQWMsRUFDL0IsYUFBYSxFQUFFLGFBQWEsRUFBRSxrQkFBa0IsRUFDaEQsb0JBQW9CLEVBQUUsaUJBQWlCLEVBQUUsaUJBQWlCLEVBQzFELGdCQUFnQixFQUFFLGVBQWUsRUFBRSxtQkFBbUIsRUFDdEQsbUJBQW1CLEVBQUUsZ0JBQWdCLEVBQUMsYUFBYSxFQUFFLGdCQUFnQixFQUFFLGdCQUFnQixFQUN4RixNQUFNLG1CQUFtQixDQUFDO0FBQzNCLE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQW9DaEUsTUFBTSxPQUFPLHVCQUF1Qjs7O1lBbENuQyxRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFO29CQUNQLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYixpQkFBaUI7b0JBQ2pCLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixrQkFBa0I7b0JBQ2xCLGNBQWM7b0JBQ2QsYUFBYTtvQkFDYixhQUFhO29CQUNiLHFCQUFxQjtvQkFDckIsY0FBYztvQkFDZCxXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGFBQWE7b0JBQ2IsYUFBYTtvQkFDYixrQkFBa0I7b0JBQ2xCLG9CQUFvQjtvQkFDcEIsaUJBQWlCO29CQUNqQixpQkFBaUI7b0JBQ2pCLGdCQUFnQjtvQkFDaEIsZUFBZTtvQkFDZixtQkFBbUI7b0JBQ25CLG1CQUFtQjtvQkFDbkIsYUFBYTtvQkFDYixnQkFBZ0I7b0JBQ2QsYUFBYTtvQkFDYixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtpQkFDbkI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtcbiAgTWF0QnV0dG9uTW9kdWxlLCBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXG4gIE1hdENhcmRNb2R1bGUsXG4gIE1hdEZvcm1GaWVsZE1vZHVsZSxcbiAgTWF0R3JpZExpc3RNb2R1bGUsIE1hdEljb25Nb2R1bGUsXG4gIE1hdElucHV0TW9kdWxlLCBNYXRMaXN0TW9kdWxlLFxuICBNYXRSYWRpb01vZHVsZSxcbiAgTWF0U2VsZWN0TW9kdWxlLCBNYXRUYWJsZU1vZHVsZSxcbiAgTWF0TWVudU1vZHVsZSwgTWF0VGFic01vZHVsZSwgTWF0RXhwYW5zaW9uTW9kdWxlLFxuICBNYXRCb3R0b21TaGVldE1vZHVsZSwgTWF0Q2hlY2tib3hNb2R1bGUsIE1hdFNuYWNrQmFyTW9kdWxlLFxuICBNYXRUb29sdGlwTW9kdWxlLCBNYXREaWFsb2dNb2R1bGUsIE1hdERhdGVwaWNrZXJNb2R1bGUsXG4gIE1hdE5hdGl2ZURhdGVNb2R1bGUsIE1hdFRvb2xiYXJNb2R1bGUsTWF0VHJlZU1vZHVsZSwgTWF0U2lkZW5hdk1vZHVsZSwgTWF0RGl2aWRlck1vZHVsZVxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0Zvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBOZ01vZHVsZSh7XG4gIGV4cG9ydHM6IFtcbiAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgTWF0Q2FyZE1vZHVsZSxcbiAgICBNYXRHcmlkTGlzdE1vZHVsZSxcbiAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgIE1hdFJhZGlvTW9kdWxlLFxuICAgIE1hdExpc3RNb2R1bGUsXG4gICAgTWF0SWNvbk1vZHVsZSxcbiAgICBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXG4gICAgTWF0VGFibGVNb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIE1hdE1lbnVNb2R1bGUsXG4gICAgTWF0VGFic01vZHVsZSxcbiAgICBNYXRFeHBhbnNpb25Nb2R1bGUsXG4gICAgTWF0Qm90dG9tU2hlZXRNb2R1bGUsXG4gICAgTWF0Q2hlY2tib3hNb2R1bGUsXG4gICAgTWF0U25hY2tCYXJNb2R1bGUsXG4gICAgTWF0VG9vbHRpcE1vZHVsZSxcbiAgICBNYXREaWFsb2dNb2R1bGUsXG4gICAgTWF0RGF0ZXBpY2tlck1vZHVsZSxcbiAgICBNYXROYXRpdmVEYXRlTW9kdWxlLFxuICAgIE1hdEljb25Nb2R1bGUsXG4gICAgTWF0VG9vbGJhck1vZHVsZSxcbiAgICAgIE1hdFRyZWVNb2R1bGUsXG4gICAgICBNYXRTaWRlbmF2TW9kdWxlLFxuICAgICAgTWF0RGl2aWRlck1vZHVsZVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIE1hdGVyaWFsQ29tcG9uZW50TW9kdWxlIHtcbn1cbiJdfQ==