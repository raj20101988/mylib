/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class NoDataAvailableComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
NoDataAvailableComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-no-data-available',
                template: "<div class=\"mdc-layout-grid no-data-available\" *ngIf=\"totalCount > 0\">\n  <h1 [translate]=\"'application.app.common.labels.NO_DATA_AVAILABLE'\">No Data Available</h1>\n</div>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
NoDataAvailableComponent.ctorParameters = () => [];
NoDataAvailableComponent.propDecorators = {
    totalCount: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NoDataAvailableComponent.prototype.totalCount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm8tZGF0YS1hdmFpbGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsibm8tZGF0YS1hdmFpbGFibGUvbm8tZGF0YS1hdmFpbGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQU92RCxNQUFNLE9BQU8sd0JBQXdCO0lBRW5DLGdCQUFnQixDQUFDOzs7O0lBRWpCLFFBQVE7SUFDUixDQUFDOzs7WUFWRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtnQkFDakMsZ01BQWlEOzthQUVsRDs7Ozs7eUJBRUUsS0FBSzs7OztJQUFOLDhDQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLW5vLWRhdGEtYXZhaWxhYmxlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL25vLWRhdGEtYXZhaWxhYmxlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbm8tZGF0YS1hdmFpbGFibGUuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBOb0RhdGFBdmFpbGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSB0b3RhbENvdW50OiBudW1iZXI7XG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cbn1cbiJdfQ==