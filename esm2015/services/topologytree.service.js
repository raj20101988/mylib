/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/**
 * @record
 */
export function ITopologyNodes() { }
if (false) {
    /** @type {?} */
    ITopologyNodes.prototype.meta;
    /** @type {?} */
    ITopologyNodes.prototype.nodeId;
    /** @type {?} */
    ITopologyNodes.prototype.name;
    /** @type {?} */
    ITopologyNodes.prototype.shortName;
    /** @type {?} */
    ITopologyNodes.prototype.type;
    /** @type {?} */
    ITopologyNodes.prototype.description;
    /** @type {?} */
    ITopologyNodes.prototype.deleted;
    /** @type {?} */
    ITopologyNodes.prototype.retired;
    /** @type {?} */
    ITopologyNodes.prototype.parentNodeId;
    /** @type {?} */
    ITopologyNodes.prototype.parentNodeHref;
    /** @type {?} */
    ITopologyNodes.prototype.path;
    /** @type {?} */
    ITopologyNodes.prototype.childNodesHrefs;
    /** @type {?} */
    ITopologyNodes.prototype.createdDtm;
    /** @type {?} */
    ITopologyNodes.prototype.updatedDtm;
    /** @type {?} */
    ITopologyNodes.prototype.status;
}
/**
 * AVG:(Access or Virtual Group) nodes, this interface has common properties for access and virtual group.
 * @record
 */
export function ITopologyAVGNodes() { }
if (false) {
    /** @type {?} */
    ITopologyAVGNodes.prototype.createdDtm;
    /** @type {?} */
    ITopologyAVGNodes.prototype.groupName;
    /** @type {?} */
    ITopologyAVGNodes.prototype.groupType;
    /** @type {?} */
    ITopologyAVGNodes.prototype.meta;
    /** @type {?} */
    ITopologyAVGNodes.prototype.topologyGroupId;
    /** @type {?} */
    ITopologyAVGNodes.prototype.topologyId;
    /** @type {?} */
    ITopologyAVGNodes.prototype.topologyNodeIds;
    /** @type {?} */
    ITopologyAVGNodes.prototype.updatedDtm;
    /** @type {?} */
    ITopologyAVGNodes.prototype.userId;
}
/**
 * if you need to provide it at global level do not specify the module here just write the 'root', this is restricted to module if you
 * want to use the service need to import TopologyTreeModule.
 */
export class TopologytreeService {
    /**
     * @param {?} _http
     */
    constructor(_http) {
        this._http = _http;
    }
    // getTopologyNode: getTopologyNode,
    // getTopologyNodeByNodeName: getTopologyNodeByNodeName,
    // getTopologyNodeByNodeId: getTopologyNodeByNodeId,
    // getTopologyNodes: getTopologyNodes,
    // getTopologyNodeByHostName: getTopologyNodeByHostName,
    // getTopologyNodesFromCache: getTopologyNodesFromCache,
    // getTopologyTypes: getTopologyTypes,
    // updateTopologyNode: updateTopologyNode,
    // getUnAssignedDevices: getUnAssignedDevices,
    // addTopologyNode: addTopologyNode,
    // deleteNode: deleteNode,
    // getTopologyNodeUnassigned: getTopologyNodeUnassigned,
    // getNodeByTopologyType: getNodeByTopologyType,
    // editTopologyNode: editTopologyNode,
    // createAccessGroup: createAccessGroup,
    // getAccessGroup: getAccessGroup,
    // getBonusGroup: getBonusGroup,
    // getAccessGroupById: getAccessGroupById,
    // updateAccessGroup: updateAccessGroup,
    // deleteAccessGroup: deleteAccessGroup,
    // getTopologyDetails: getTopologyDetails,
    // updateTableStatus:updateTableStatus,
    // getDescendantNodesOfTGroup: getDescendantNodesOfTGroup
    /**
     * @param {?} url
     * @return {?}
     */
    getTopologyNodes(url) {
        return (/** @type {?} */ (this._http.get(url)));
    }
    /**
     * @param {?} url
     * @return {?}
     */
    getTopologyTypes(url) {
        return (/** @type {?} */ (this._http.get(url)));
    }
    /**
     * @param {?} url
     * @param {?=} params
     * @return {?}
     */
    getTopologyNodeByNodeId(url, params = null) {
        return (/** @type {?} */ (this._http.get(url, { params })));
    }
    /**
     * @param {?} url
     * @param {?=} params
     * @return {?}
     */
    getAccessOrVirtualGroupNodes(url, params = null) {
        return (/** @type {?} */ (this._http.get(url, { params })));
    }
    /**
     * @param {?} url
     * @return {?}
     */
    getTopologyNodeUnassigned(url) {
        return (/** @type {?} */ (this._http.get(url)));
    }
}
TopologytreeService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TopologytreeService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ TopologytreeService.ngInjectableDef = i0.defineInjectable({ factory: function TopologytreeService_Factory() { return new TopologytreeService(i0.inject(i1.HttpClient)); }, token: TopologytreeService, providedIn: "root" });
if (false) {
    /** @type {?} */
    TopologytreeService.prototype._http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9wb2xvZ3l0cmVlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJzZXJ2aWNlcy90b3BvbG9neXRyZWUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUMsVUFBVSxFQUEyQixNQUFNLHNCQUFzQixDQUFDOzs7Ozs7QUFHMUUsb0NBbUJDOzs7SUFsQkcsOEJBR0U7O0lBQ0YsZ0NBQWU7O0lBQ2YsOEJBQWE7O0lBQ2IsbUNBQWtCOztJQUNsQiw4QkFBYTs7SUFDYixxQ0FBb0I7O0lBQ3BCLGlDQUFpQjs7SUFDakIsaUNBQWlCOztJQUNqQixzQ0FBcUI7O0lBQ3JCLHdDQUF1Qjs7SUFDdkIsOEJBQWE7O0lBQ2IseUNBQTRCOztJQUM1QixvQ0FBbUI7O0lBQ25CLG9DQUFtQjs7SUFDbkIsZ0NBQWU7Ozs7OztBQU9uQix1Q0FhQzs7O0lBWkcsdUNBQW1COztJQUNuQixzQ0FBa0I7O0lBQ2xCLHNDQUFrQjs7SUFDbEIsaUNBR0U7O0lBQ0YsNENBQXdCOztJQUN4Qix1Q0FBbUI7O0lBQ25CLDRDQUE0Qjs7SUFDNUIsdUNBQW1COztJQUNuQixtQ0FBZTs7Ozs7O0FBVW5CLE1BQU0sT0FBTyxtQkFBbUI7Ozs7SUFDOUIsWUFBb0IsS0FBaUI7UUFBakIsVUFBSyxHQUFMLEtBQUssQ0FBWTtJQUFJLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUF5QnhDLGdCQUFnQixDQUFDLEdBQUc7UUFDaEIsT0FBTyxtQkFBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBZ0MsQ0FBQztJQUMvRCxDQUFDOzs7OztJQUNELGdCQUFnQixDQUFDLEdBQUc7UUFDaEIsT0FBTyxtQkFBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBZ0MsQ0FBQztJQUMvRCxDQUFDOzs7Ozs7SUFFRCx1QkFBdUIsQ0FBQyxHQUFHLEVBQUUsU0FBcUIsSUFBSTtRQUNsRCxPQUFPLG1CQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQWdDLENBQUM7SUFDM0UsQ0FBQzs7Ozs7O0lBQ0QsNEJBQTRCLENBQUMsR0FBRyxFQUFFLFNBQXFCLElBQUk7UUFDdkQsT0FBTyxtQkFBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFtQyxDQUFDO0lBQzlFLENBQUM7Ozs7O0lBQ0QseUJBQXlCLENBQUMsR0FBRztRQUN6QixPQUFPLG1CQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFnQyxDQUFDO0lBQy9ELENBQUM7OztZQTVDSixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFqRE8sVUFBVTs7Ozs7SUFtREosb0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwQ2xpZW50LCBIdHRwUGFyYW1zLCBIdHRwUmVzcG9uc2V9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVRvcG9sb2d5Tm9kZXMge1xuICAgIG1ldGE6IHtcbiAgICAgICAgaHJlZjogc3RyaW5nLFxuICAgICAgICBtZWRpYVR5cGU6IHN0cmluZ1xuICAgIH07XG4gICAgbm9kZUlkOiBzdHJpbmc7XG4gICAgbmFtZTogc3RyaW5nO1xuICAgIHNob3J0TmFtZTogc3RyaW5nO1xuICAgIHR5cGU6IG51bWJlcjtcbiAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xuICAgIGRlbGV0ZWQ6IGJvb2xlYW47XG4gICAgcmV0aXJlZDogYm9vbGVhbjtcbiAgICBwYXJlbnROb2RlSWQ6IG51bWJlcjtcbiAgICBwYXJlbnROb2RlSHJlZjogc3RyaW5nO1xuICAgIHBhdGg6IHN0cmluZztcbiAgICBjaGlsZE5vZGVzSHJlZnM6IEFycmF5PGFueT47XG4gICAgY3JlYXRlZER0bTogc3RyaW5nO1xuICAgIHVwZGF0ZWREdG06IHN0cmluZztcbiAgICBzdGF0dXM6IHN0cmluZztcbn1cblxuXG4vKipcbiAqIEFWRzooQWNjZXNzIG9yIFZpcnR1YWwgR3JvdXApIG5vZGVzLCB0aGlzIGludGVyZmFjZSBoYXMgY29tbW9uIHByb3BlcnRpZXMgZm9yIGFjY2VzcyBhbmQgdmlydHVhbCBncm91cC5cbiAqL1xuZXhwb3J0IGludGVyZmFjZSBJVG9wb2xvZ3lBVkdOb2RlcyB7XG4gICAgY3JlYXRlZER0bTogc3RyaW5nO1xuICAgIGdyb3VwTmFtZTogc3RyaW5nO1xuICAgIGdyb3VwVHlwZTogc3RyaW5nO1xuICAgIG1ldGE6IHtcbiAgICAgICAgaHJlZjogc3RyaW5nLFxuICAgICAgICBtZWRpYVR5cGU6IHN0cmluZ1xuICAgIH07XG4gICAgdG9wb2xvZ3lHcm91cElkOiBudW1iZXI7XG4gICAgdG9wb2xvZ3lJZDogbnVtYmVyO1xuICAgIHRvcG9sb2d5Tm9kZUlkczogQXJyYXk8YW55PjtcbiAgICB1cGRhdGVkRHRtOiBzdHJpbmc7XG4gICAgdXNlcklkOiBudW1iZXI7XG59XG5cbi8qKlxuICogaWYgeW91IG5lZWQgdG8gcHJvdmlkZSBpdCBhdCBnbG9iYWwgbGV2ZWwgZG8gbm90IHNwZWNpZnkgdGhlIG1vZHVsZSBoZXJlIGp1c3Qgd3JpdGUgdGhlICdyb290JywgdGhpcyBpcyByZXN0cmljdGVkIHRvIG1vZHVsZSBpZiB5b3VcbiAqIHdhbnQgdG8gdXNlIHRoZSBzZXJ2aWNlIG5lZWQgdG8gaW1wb3J0IFRvcG9sb2d5VHJlZU1vZHVsZS5cbiAqL1xuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVG9wb2xvZ3l0cmVlU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX2h0dHA6IEh0dHBDbGllbnQpIHsgfVxuXG4gICAgLy8gZ2V0VG9wb2xvZ3lOb2RlOiBnZXRUb3BvbG9neU5vZGUsXG4gICAgLy8gZ2V0VG9wb2xvZ3lOb2RlQnlOb2RlTmFtZTogZ2V0VG9wb2xvZ3lOb2RlQnlOb2RlTmFtZSxcbiAgICAvLyBnZXRUb3BvbG9neU5vZGVCeU5vZGVJZDogZ2V0VG9wb2xvZ3lOb2RlQnlOb2RlSWQsXG4gICAgLy8gZ2V0VG9wb2xvZ3lOb2RlczogZ2V0VG9wb2xvZ3lOb2RlcyxcbiAgICAvLyBnZXRUb3BvbG9neU5vZGVCeUhvc3ROYW1lOiBnZXRUb3BvbG9neU5vZGVCeUhvc3ROYW1lLFxuICAgIC8vIGdldFRvcG9sb2d5Tm9kZXNGcm9tQ2FjaGU6IGdldFRvcG9sb2d5Tm9kZXNGcm9tQ2FjaGUsXG4gICAgLy8gZ2V0VG9wb2xvZ3lUeXBlczogZ2V0VG9wb2xvZ3lUeXBlcyxcbiAgICAvLyB1cGRhdGVUb3BvbG9neU5vZGU6IHVwZGF0ZVRvcG9sb2d5Tm9kZSxcbiAgICAvLyBnZXRVbkFzc2lnbmVkRGV2aWNlczogZ2V0VW5Bc3NpZ25lZERldmljZXMsXG4gICAgLy8gYWRkVG9wb2xvZ3lOb2RlOiBhZGRUb3BvbG9neU5vZGUsXG4gICAgLy8gZGVsZXRlTm9kZTogZGVsZXRlTm9kZSxcbiAgICAvLyBnZXRUb3BvbG9neU5vZGVVbmFzc2lnbmVkOiBnZXRUb3BvbG9neU5vZGVVbmFzc2lnbmVkLFxuICAgIC8vIGdldE5vZGVCeVRvcG9sb2d5VHlwZTogZ2V0Tm9kZUJ5VG9wb2xvZ3lUeXBlLFxuICAgIC8vIGVkaXRUb3BvbG9neU5vZGU6IGVkaXRUb3BvbG9neU5vZGUsXG4gICAgLy8gY3JlYXRlQWNjZXNzR3JvdXA6IGNyZWF0ZUFjY2Vzc0dyb3VwLFxuICAgIC8vIGdldEFjY2Vzc0dyb3VwOiBnZXRBY2Nlc3NHcm91cCxcbiAgICAvLyBnZXRCb251c0dyb3VwOiBnZXRCb251c0dyb3VwLFxuICAgIC8vIGdldEFjY2Vzc0dyb3VwQnlJZDogZ2V0QWNjZXNzR3JvdXBCeUlkLFxuICAgIC8vIHVwZGF0ZUFjY2Vzc0dyb3VwOiB1cGRhdGVBY2Nlc3NHcm91cCxcbiAgICAvLyBkZWxldGVBY2Nlc3NHcm91cDogZGVsZXRlQWNjZXNzR3JvdXAsXG4gICAgLy8gZ2V0VG9wb2xvZ3lEZXRhaWxzOiBnZXRUb3BvbG9neURldGFpbHMsXG4gICAgLy8gdXBkYXRlVGFibGVTdGF0dXM6dXBkYXRlVGFibGVTdGF0dXMsXG4gICAgLy8gZ2V0RGVzY2VuZGFudE5vZGVzT2ZUR3JvdXA6IGdldERlc2NlbmRhbnROb2Rlc09mVEdyb3VwXG4gICAgZ2V0VG9wb2xvZ3lOb2Rlcyh1cmwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KHVybCkgYXMgT2JzZXJ2YWJsZTxJVG9wb2xvZ3lOb2Rlc1tdPjtcbiAgICB9XG4gICAgZ2V0VG9wb2xvZ3lUeXBlcyh1cmwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KHVybCkgYXMgT2JzZXJ2YWJsZTxJVG9wb2xvZ3lOb2Rlc1tdPjtcbiAgICB9XG5cbiAgICBnZXRUb3BvbG9neU5vZGVCeU5vZGVJZCh1cmwsIHBhcmFtczogSHR0cFBhcmFtcyA9IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KHVybCwgeyBwYXJhbXMgfSkgYXMgT2JzZXJ2YWJsZTxJVG9wb2xvZ3lOb2Rlc1tdPjtcbiAgICB9XG4gICAgZ2V0QWNjZXNzT3JWaXJ0dWFsR3JvdXBOb2Rlcyh1cmwsIHBhcmFtczogSHR0cFBhcmFtcyA9IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KHVybCwgeyBwYXJhbXMgfSkgYXMgT2JzZXJ2YWJsZTxJVG9wb2xvZ3lBVkdOb2Rlc1tdPjtcbiAgICB9XG4gICAgZ2V0VG9wb2xvZ3lOb2RlVW5hc3NpZ25lZCh1cmwpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KHVybCkgYXMgT2JzZXJ2YWJsZTxJVG9wb2xvZ3lOb2Rlc1tdPjtcbiAgICB9XG59XG4iXX0=