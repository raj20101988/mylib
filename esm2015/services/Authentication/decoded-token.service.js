/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as i0 from "@angular/core";
import * as i1 from "@auth0/angular-jwt/src/jwthelper.service";
export class DecodedTokenService {
    /**
     * @param {?} jwtHelper
     */
    constructor(jwtHelper) {
        this.jwtHelper = jwtHelper;
    }
    /**
     * @return {?}
     */
    getDecodedJwtToken() {
        return this.decodedJwtToken;
    }
    /**
     * @param {?} token
     * @return {?}
     */
    setDecodedJwtToken(token) {
        this.decodedJwtToken = this.jwtHelper.decodeToken(token);
    }
}
DecodedTokenService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DecodedTokenService.ctorParameters = () => [
    { type: JwtHelperService }
];
/** @nocollapse */ DecodedTokenService.ngInjectableDef = i0.defineInjectable({ factory: function DecodedTokenService_Factory() { return new DecodedTokenService(i0.inject(i1.JwtHelperService)); }, token: DecodedTokenService, providedIn: "root" });
if (false) {
    /** @type {?} */
    DecodedTokenService.prototype.decodedJwtToken;
    /** @type {?} */
    DecodedTokenService.prototype.jwtHelper;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVjb2RlZC10b2tlbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsic2VydmljZXMvQXV0aGVudGljYXRpb24vZGVjb2RlZC10b2tlbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLG9CQUFvQixDQUFDOzs7QUFLcEQsTUFBTSxPQUFPLG1CQUFtQjs7OztJQUU1QixZQUFvQixTQUEyQjtRQUEzQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtJQUMvQyxDQUFDOzs7O0lBSUQsa0JBQWtCO1FBQ2QsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRUQsa0JBQWtCLENBQUMsS0FBSztRQUNwQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzdELENBQUM7OztZQWhCSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFKTyxnQkFBZ0I7Ozs7O0lBVXBCLDhDQUF3Qjs7SUFIWix3Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtKd3RIZWxwZXJTZXJ2aWNlfSBmcm9tICdAYXV0aDAvYW5ndWxhci1qd3QnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIERlY29kZWRUb2tlblNlcnZpY2Uge1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBqd3RIZWxwZXI6IEp3dEhlbHBlclNlcnZpY2UpIHtcbiAgICB9XG5cbiAgICBwcml2YXRlIGRlY29kZWRKd3RUb2tlbjtcblxuICAgIGdldERlY29kZWRKd3RUb2tlbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGVjb2RlZEp3dFRva2VuO1xuICAgIH1cblxuICAgIHNldERlY29kZWRKd3RUb2tlbih0b2tlbikge1xuICAgICAgICB0aGlzLmRlY29kZWRKd3RUb2tlbiA9IHRoaXMuand0SGVscGVyLmRlY29kZVRva2VuKHRva2VuKTtcbiAgICB9XG59XG4iXX0=