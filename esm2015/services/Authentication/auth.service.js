/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { urls } from '../../constants/urls';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class AuthService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
    }
    /**
     * @param {?} clientId
     * @return {?}
     */
    getRefreshToken(clientId) {
        /** @type {?} */
        const authValues = JSON.parse(sessionStorage.getItem('authValues'));
        /** @type {?} */
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json', 'Authorization': 'Bearer ' +
                    sessionStorage.getItem(authValues.jwtTokenKey)
            }),
            params: {
                client_id: clientId
            }
        };
        return this.http.post(urls.auth.refresh, null, httpOptions);
    }
}
AuthService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ AuthService.ngInjectableDef = i0.defineInjectable({ factory: function AuthService_Factory() { return new AuthService(i0.inject(i1.HttpClient)); }, token: AuthService, providedIn: "root" });
if (false) {
    /** @type {?} */
    AuthService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsic2VydmljZXMvQXV0aGVudGljYXRpb24vYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxVQUFVLEVBQUUsV0FBVyxFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFFN0QsT0FBTyxFQUFDLElBQUksRUFBQyxNQUFNLHNCQUFzQixDQUFDOzs7QUFLMUMsTUFBTSxPQUFPLFdBQVc7Ozs7SUFFcEIsWUFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtJQUNwQyxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxRQUFROztjQUNkLFVBQVUsR0FBUSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7O2NBQ2xFLFdBQVcsR0FBRztZQUNoQixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3JCLGNBQWMsRUFBRSxrQkFBa0IsRUFBRSxlQUFlLEVBQUUsU0FBUztvQkFDMUQsY0FBYyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO2FBQ3JELENBQUM7WUFDRixNQUFNLEVBQUU7Z0JBQ0osU0FBUyxFQUFFLFFBQVE7YUFDdEI7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7OztZQXBCSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFOTyxVQUFVOzs7OztJQVNGLDJCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBDbGllbnQsIEh0dHBIZWFkZXJzfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHt1cmxzfSBmcm9tICcuLi8uLi9jb25zdGFudHMvdXJscyc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2Uge1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7XG4gICAgfVxuXG4gICAgZ2V0UmVmcmVzaFRva2VuKGNsaWVudElkKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICAgICAgY29uc3QgYXV0aFZhbHVlczogYW55ID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCdhdXRoVmFsdWVzJykpO1xuICAgICAgICBjb25zdCBodHRwT3B0aW9ucyA9IHtcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJywgJ0F1dGhvcml6YXRpb24nOiAnQmVhcmVyICcgK1xuICAgICAgICAgICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGF1dGhWYWx1ZXMuand0VG9rZW5LZXkpXG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIHBhcmFtczoge1xuICAgICAgICAgICAgICAgIGNsaWVudF9pZDogY2xpZW50SWRcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHVybHMuYXV0aC5yZWZyZXNoLCBudWxsLCBodHRwT3B0aW9ucyk7XG4gICAgfVxufVxuIl19