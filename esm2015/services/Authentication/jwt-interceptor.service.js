/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as i0 from "@angular/core";
import * as i1 from "@auth0/angular-jwt/src/jwthelper.service";
/**
 * This service adds jwt token on every API call
 */
export class JwtInterceptorService {
    /**
     * @param {?} jwtHelper
     */
    constructor(jwtHelper) {
        this.jwtHelper = jwtHelper;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        /** @type {?} */
        const jwtToken = this.jwtHelper.tokenGetter();
        req = req.clone({
            setHeaders: {
                Authorization: `Bearer ${jwtToken}`
            }
        });
        return next.handle(req);
    }
}
JwtInterceptorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
JwtInterceptorService.ctorParameters = () => [
    { type: JwtHelperService }
];
/** @nocollapse */ JwtInterceptorService.ngInjectableDef = i0.defineInjectable({ factory: function JwtInterceptorService_Factory() { return new JwtInterceptorService(i0.inject(i1.JwtHelperService)); }, token: JwtInterceptorService, providedIn: "root" });
if (false) {
    /** @type {?} */
    JwtInterceptorService.prototype.jwtHelper;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LWludGVyY2VwdG9yLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9BdXRoZW50aWNhdGlvbi9qd3QtaW50ZXJjZXB0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUd6QyxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQzs7Ozs7O0FBU3BELE1BQU0sT0FBTyxxQkFBcUI7Ozs7SUFFOUIsWUFBb0IsU0FBMkI7UUFBM0IsY0FBUyxHQUFULFNBQVMsQ0FBa0I7SUFDL0MsQ0FBQzs7Ozs7O0lBRUQsU0FBUyxDQUFDLEdBQXFCLEVBQUUsSUFBaUI7O2NBQ3hDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRTtRQUM3QyxHQUFHLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUNaLFVBQVUsRUFBRTtnQkFDUixhQUFhLEVBQUUsVUFBVSxRQUFRLEVBQUU7YUFDdEM7U0FDSixDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7O1lBakJKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQVJPLGdCQUFnQjs7Ozs7SUFXUiwwQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtIdHRwRXZlbnQsIEh0dHBIYW5kbGVyLCBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXF1ZXN0fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtKd3RIZWxwZXJTZXJ2aWNlfSBmcm9tICdAYXV0aDAvYW5ndWxhci1qd3QnO1xuXG4vKipcbiAqIFRoaXMgc2VydmljZSBhZGRzIGp3dCB0b2tlbiBvbiBldmVyeSBBUEkgY2FsbFxuICovXG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgSnd0SW50ZXJjZXB0b3JTZXJ2aWNlIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgand0SGVscGVyOiBKd3RIZWxwZXJTZXJ2aWNlKSB7XG4gICAgfVxuXG4gICAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgICAgIGNvbnN0IGp3dFRva2VuID0gdGhpcy5qd3RIZWxwZXIudG9rZW5HZXR0ZXIoKTtcbiAgICAgICAgcmVxID0gcmVxLmNsb25lKHtcbiAgICAgICAgICAgIHNldEhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7and0VG9rZW59YFxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxKTtcbiAgICB9XG5cbn1cbiJdfQ==