/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { protocol, webServerDNS } from '../../constants/url.common.constants';
import * as i0 from "@angular/core";
import * as i1 from "@auth0/angular-jwt/src/jwthelper.service";
export class TokenHandlingService {
    /**
     * @param {?} jwtHelper
     */
    constructor(jwtHelper) {
        this.jwtHelper = jwtHelper;
        this.authValues = JSON.parse(sessionStorage.getItem('authValues'));
    }
    /**
     * @return {?}
     */
    redirectURI() {
        location.href = `${protocol}${webServerDNS}` + '/ldaplogin.html' +
            '?client_id=' + this.authValues.clientId + '&redirect_uri=' + location.href;
    }
    /**
     * @param {?} jwtToken
     * @return {?}
     */
    isTokenValid(jwtToken) {
        return this.hasPermission(jwtToken) && !this.jwtHelper.isTokenExpired(jwtToken);
    }
    /**
     * @param {?} jwtToken
     * @return {?}
     */
    hasPermission(jwtToken) {
        /** @type {?} */
        const decodedToken = this.jwtHelper.decodeToken(jwtToken);
        if (decodedToken) {
            if (decodedToken.authorities && ((this.authValues.applicationCode === decodedToken.authorities[0].applicationCode &&
                decodedToken.authorities[0].permissions.includes(this.authValues.accessCode)) || decodedToken.superuser)) {
                return true;
            }
            else {
                return !!(this.authValues.clientId === 'lgn' && decodedToken.applications);
            }
        }
        else {
            return false;
        }
    }
}
TokenHandlingService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TokenHandlingService.ctorParameters = () => [
    { type: JwtHelperService }
];
/** @nocollapse */ TokenHandlingService.ngInjectableDef = i0.defineInjectable({ factory: function TokenHandlingService_Factory() { return new TokenHandlingService(i0.inject(i1.JwtHelperService)); }, token: TokenHandlingService, providedIn: "root" });
if (false) {
    /** @type {?} */
    TokenHandlingService.prototype.authValues;
    /** @type {?} */
    TokenHandlingService.prototype.jwtHelper;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9rZW4taGFuZGxpbmcuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL0F1dGhlbnRpY2F0aW9uL3Rva2VuLWhhbmRsaW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQzs7O0FBSzVFLE1BQU0sT0FBTyxvQkFBb0I7Ozs7SUFHN0IsWUFBb0IsU0FBMkI7UUFBM0IsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLFFBQVEsQ0FBQyxJQUFJLEdBQUcsR0FBRyxRQUFRLEdBQUcsWUFBWSxFQUFFLEdBQUcsaUJBQWlCO1lBQzVELGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO0lBQ3BGLENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLFFBQVE7UUFDakIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEYsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsUUFBUTs7Y0FDWixZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQ3pELElBQUksWUFBWSxFQUFFO1lBQ2QsSUFBSSxZQUFZLENBQUMsV0FBVyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsS0FBSyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLGVBQWU7Z0JBQzdHLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksWUFBWSxDQUFDLFNBQVMsQ0FBQyxFQUFFO2dCQUMxRyxPQUFPLElBQUksQ0FBQzthQUNmO2lCQUFNO2dCQUNILE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEtBQUssS0FBSyxJQUFJLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUM5RTtTQUNKO2FBQU07WUFDSCxPQUFPLEtBQUssQ0FBQztTQUNoQjtJQUNMLENBQUM7OztZQS9CSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFMTyxnQkFBZ0I7Ozs7O0lBT3BCLDBDQUFnQjs7SUFFSix5Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtKd3RIZWxwZXJTZXJ2aWNlfSBmcm9tICdAYXV0aDAvYW5ndWxhci1qd3QnO1xuaW1wb3J0IHtwcm90b2NvbCwgd2ViU2VydmVyRE5TfSBmcm9tICcuLi8uLi9jb25zdGFudHMvdXJsLmNvbW1vbi5jb25zdGFudHMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFRva2VuSGFuZGxpbmdTZXJ2aWNlIHtcbiAgICBhdXRoVmFsdWVzOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGp3dEhlbHBlcjogSnd0SGVscGVyU2VydmljZSkge1xuICAgICAgICB0aGlzLmF1dGhWYWx1ZXMgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oJ2F1dGhWYWx1ZXMnKSk7XG4gICAgfVxuXG4gICAgcmVkaXJlY3RVUkkoKTogdm9pZCB7XG4gICAgICAgIGxvY2F0aW9uLmhyZWYgPSBgJHtwcm90b2NvbH0ke3dlYlNlcnZlckROU31gICsgJy9sZGFwbG9naW4uaHRtbCcgK1xuICAgICAgICAgICAgJz9jbGllbnRfaWQ9JyArIHRoaXMuYXV0aFZhbHVlcy5jbGllbnRJZCArICcmcmVkaXJlY3RfdXJpPScgKyBsb2NhdGlvbi5ocmVmO1xuICAgIH1cblxuICAgIGlzVG9rZW5WYWxpZChqd3RUb2tlbik6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5oYXNQZXJtaXNzaW9uKGp3dFRva2VuKSAmJiAhdGhpcy5qd3RIZWxwZXIuaXNUb2tlbkV4cGlyZWQoand0VG9rZW4pO1xuICAgIH1cblxuICAgIGhhc1Blcm1pc3Npb24oand0VG9rZW4pOiBib29sZWFuIHtcbiAgICAgICAgY29uc3QgZGVjb2RlZFRva2VuID0gdGhpcy5qd3RIZWxwZXIuZGVjb2RlVG9rZW4oand0VG9rZW4pO1xuICAgICAgICBpZiAoZGVjb2RlZFRva2VuKSB7XG4gICAgICAgICAgICBpZiAoZGVjb2RlZFRva2VuLmF1dGhvcml0aWVzICYmICgodGhpcy5hdXRoVmFsdWVzLmFwcGxpY2F0aW9uQ29kZSA9PT0gZGVjb2RlZFRva2VuLmF1dGhvcml0aWVzWzBdLmFwcGxpY2F0aW9uQ29kZSAmJlxuICAgICAgICAgICAgICAgIGRlY29kZWRUb2tlbi5hdXRob3JpdGllc1swXS5wZXJtaXNzaW9ucy5pbmNsdWRlcyh0aGlzLmF1dGhWYWx1ZXMuYWNjZXNzQ29kZSkpIHx8IGRlY29kZWRUb2tlbi5zdXBlcnVzZXIpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiAhISh0aGlzLmF1dGhWYWx1ZXMuY2xpZW50SWQgPT09ICdsZ24nICYmIGRlY29kZWRUb2tlbi5hcHBsaWNhdGlvbnMpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19