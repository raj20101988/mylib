/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
export class AlertService {
    /**
     * @param {?} _http
     */
    constructor(_http) {
        this._http = _http;
    }
    /**
     * @param {?} url
     * @param {?} options
     * @return {?}
     */
    getAlertsData(url, options) {
        return this._http.get(url, options);
    }
    /**
     * @return {?}
     */
    getEventData() {
    }
    /**
     * @return {?}
     */
    getAlertCounts() {
    }
    /**
     * @param {?} statusUrl
     * @param {?} statusObj
     * @return {?}
     */
    changeStatus(statusUrl, statusObj) {
        return this._http.post(statusUrl, statusObj);
    }
    /**
     * @return {?}
     */
    changeSeverity() {
    }
    /**
     * @param {?} noteUrl
     * @param {?} noteObj
     * @return {?}
     */
    addNote(noteUrl, noteObj) {
        return this._http.post(noteUrl, noteObj);
    }
    /**
     * @param {?} baseUrl
     * @param {?} baseObj
     * @return {?}
     */
    saveCustomerKnowledgeBase(baseUrl, baseObj) {
        return this._http.post(baseUrl, baseObj);
    }
}
AlertService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AlertService.ctorParameters = () => [
    { type: HttpClient }
];
if (false) {
    /** @type {?} */
    AlertService.prototype._http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2FsZXJ0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBSTlELE1BQU0sT0FBTyxZQUFZOzs7O0lBQ3JCLFlBQW9CLEtBQWlCO1FBQWpCLFVBQUssR0FBTCxLQUFLLENBQVk7SUFDckMsQ0FBQzs7Ozs7O0lBRUQsYUFBYSxDQUFDLEdBQUcsRUFBRSxPQUFXO1FBQzFCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQXVCLEdBQUcsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUM5RCxDQUFDOzs7O0lBRUQsWUFBWTtJQUVaLENBQUM7Ozs7SUFFRCxjQUFjO0lBRWQsQ0FBQzs7Ozs7O0lBRUQsWUFBWSxDQUFDLFNBQVMsRUFBRSxTQUFTO1FBQzdCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQXVCLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7O0lBRUQsY0FBYztJQUVkLENBQUM7Ozs7OztJQUVELE9BQU8sQ0FBQyxPQUFPLEVBQUUsT0FBTztRQUNwQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUF1QixPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDbkUsQ0FBQzs7Ozs7O0lBRUQseUJBQXlCLENBQUMsT0FBTyxFQUFFLE9BQU87UUFDdEMsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBdUIsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ25FLENBQUM7OztZQS9CSixVQUFVOzs7O1lBSEgsVUFBVTs7OztJQUtGLDZCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBDbGllbnQsIEh0dHBSZXNwb25zZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tICdyeGpzJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEFsZXJ0U2VydmljZSB7XG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfaHR0cDogSHR0cENsaWVudCkge1xuICAgIH1cblxuICAgIGdldEFsZXJ0c0RhdGEodXJsLCBvcHRpb25zOiB7fSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPE9iamVjdD4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0PEh0dHBSZXNwb25zZTxPYmplY3Q+Pih1cmwsIG9wdGlvbnMpO1xuICAgIH1cblxuICAgIGdldEV2ZW50RGF0YSgpOiB2b2lkIHtcblxuICAgIH1cblxuICAgIGdldEFsZXJ0Q291bnRzKCk6IHZvaWQge1xuXG4gICAgfVxuXG4gICAgY2hhbmdlU3RhdHVzKHN0YXR1c1VybCwgc3RhdHVzT2JqKTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8T2JqZWN0Pj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5wb3N0PEh0dHBSZXNwb25zZTxPYmplY3Q+PihzdGF0dXNVcmwsIHN0YXR1c09iaik7XG4gICAgfVxuXG4gICAgY2hhbmdlU2V2ZXJpdHkoKTogdm9pZCB7XG5cbiAgICB9XG5cbiAgICBhZGROb3RlKG5vdGVVcmwsIG5vdGVPYmopOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxPYmplY3Q+PiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9odHRwLnBvc3Q8SHR0cFJlc3BvbnNlPE9iamVjdD4+KG5vdGVVcmwsIG5vdGVPYmopO1xuICAgIH1cblxuICAgIHNhdmVDdXN0b21lcktub3dsZWRnZUJhc2UoYmFzZVVybCwgYmFzZU9iaik6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPE9iamVjdD4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAucG9zdDxIdHRwUmVzcG9uc2U8T2JqZWN0Pj4oYmFzZVVybCwgYmFzZU9iaik7XG4gICAgfVxuXG5cbn1cbiJdfQ==