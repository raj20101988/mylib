/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/**
 * @record
 */
export function UserSearchComplex() { }
if (false) {
    /** @type {?} */
    UserSearchComplex.prototype.authHistoryId;
    /** @type {?} */
    UserSearchComplex.prototype.creationDate;
    /** @type {?} */
    UserSearchComplex.prototype.employeeId;
    /** @type {?} */
    UserSearchComplex.prototype.firstName;
    /** @type {?} */
    UserSearchComplex.prototype.ip;
    /** @type {?} */
    UserSearchComplex.prototype.lastName;
    /** @type {?} */
    UserSearchComplex.prototype.location;
    /** @type {?} */
    UserSearchComplex.prototype.meta;
    /** @type {?} */
    UserSearchComplex.prototype.title;
    /** @type {?} */
    UserSearchComplex.prototype.type;
    /** @type {?} */
    UserSearchComplex.prototype.userId;
    /** @type {?} */
    UserSearchComplex.prototype.userName;
    /** @type {?} */
    UserSearchComplex.prototype.userType;
}
/**
 * @record
 */
export function UserSearchNormal() { }
if (false) {
    /** @type {?} */
    UserSearchNormal.prototype.employeeNumber;
    /** @type {?} */
    UserSearchNormal.prototype.firstName;
    /** @type {?} */
    UserSearchNormal.prototype.isActive;
    /** @type {?} */
    UserSearchNormal.prototype.language;
    /** @type {?} */
    UserSearchNormal.prototype.lastName;
    /** @type {?} */
    UserSearchNormal.prototype.roles;
    /** @type {?} */
    UserSearchNormal.prototype.title;
    /** @type {?} */
    UserSearchNormal.prototype.userAccessGroups;
    /** @type {?} */
    UserSearchNormal.prototype.userId;
    /** @type {?} */
    UserSearchNormal.prototype.userName;
}
/**
 *  providedIn: 'root', }) export class UserService { } 'root' means that we want provide the service at the root level (AppModule)
 *  When you provide the service at the root level, Angular creates a single, shared instance of service and injects into any class
 *  that asks for it.
 */
export class UserService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.userSearchComplexUrl = '';
        this.userSearchNormalUrl = '';
    }
    /**
     * @return {?}
     */
    getComplexSearchedUsers() {
        return (/** @type {?} */ (this.http.get(this.userSearchComplexUrl)));
    }
    /**
     * @return {?}
     */
    getNormalSearchedUsers() {
        return (/** @type {?} */ (this.http.get(this.userSearchNormalUrl)));
    }
    /**
     * @param {?} uri
     * @param {?} paramObj
     * @return {?}
     */
    setFavoriteApp(uri, paramObj) {
        return this.http.post(uri, paramObj);
    }
}
UserService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
UserService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ UserService.ngInjectableDef = i0.defineInjectable({ factory: function UserService_Factory() { return new UserService(i0.inject(i1.HttpClient)); }, token: UserService, providedIn: "root" });
if (false) {
    /** @type {?} */
    UserService.prototype.userSearchComplexUrl;
    /** @type {?} */
    UserService.prototype.userSearchNormalUrl;
    /** @type {?} */
    UserService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsic2VydmljZXMvdXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQzs7Ozs7O0FBR2hELHVDQWNDOzs7SUFiQywwQ0FBc0I7O0lBQ3RCLHlDQUFxQjs7SUFDckIsdUNBQW1COztJQUNuQixzQ0FBa0I7O0lBQ2xCLCtCQUFXOztJQUNYLHFDQUFpQjs7SUFDakIscUNBQWlCOztJQUNqQixpQ0FBYTs7SUFDYixrQ0FBYzs7SUFDZCxpQ0FBYTs7SUFDYixtQ0FBZTs7SUFDZixxQ0FBaUI7O0lBQ2pCLHFDQUFpQjs7Ozs7QUFHbkIsc0NBV0M7OztJQVZDLDBDQUF1Qjs7SUFDdkIscUNBQWtCOztJQUNsQixvQ0FBaUI7O0lBQ2pCLG9DQUFpQjs7SUFDakIsb0NBQWlCOztJQUNqQixpQ0FBa0I7O0lBQ2xCLGlDQUFjOztJQUNkLDRDQUE2Qjs7SUFDN0Isa0NBQWU7O0lBQ2Ysb0NBQWlCOzs7Ozs7O0FBV25CLE1BQU0sT0FBTyxXQUFXOzs7O0lBR3RCLFlBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFGcEMseUJBQW9CLEdBQUcsRUFBRSxDQUFDO1FBQzFCLHdCQUFtQixHQUFHLEVBQUUsQ0FBQztJQUNlLENBQUM7Ozs7SUFDekMsdUJBQXVCO1FBQ3JCLE9BQU8sbUJBQUEsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEVBQW1DLENBQUM7SUFDckYsQ0FBQzs7OztJQUNELHNCQUFzQjtRQUNuQixPQUFPLG1CQUFBLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxFQUFrQyxDQUFDO0lBQ3BGLENBQUM7Ozs7OztJQUVELGNBQWMsQ0FBQyxHQUFHLEVBQUUsUUFBUTtRQUMxQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7WUFoQkYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBdkNPLFVBQVU7Ozs7O0lBeUNoQiwyQ0FBMEI7O0lBQzFCLDBDQUF5Qjs7SUFDYiwyQkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBDbGllbnR9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgVXNlclNlYXJjaENvbXBsZXgge1xuICBhdXRoSGlzdG9yeUlkOiBudW1iZXI7XG4gIGNyZWF0aW9uRGF0ZTogc3RyaW5nO1xuICBlbXBsb3llZUlkOiBudW1iZXI7XG4gIGZpcnN0TmFtZTogc3RyaW5nO1xuICBpcDogc3RyaW5nO1xuICBsYXN0TmFtZTogc3RyaW5nO1xuICBsb2NhdGlvbjogc3RyaW5nO1xuICBtZXRhOiBzdHJpbmc7XG4gIHRpdGxlOiBzdHJpbmc7XG4gIHR5cGU6IHN0cmluZztcbiAgdXNlcklkOiBudW1iZXI7XG4gIHVzZXJOYW1lOiBzdHJpbmc7XG4gIHVzZXJUeXBlOiBzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgVXNlclNlYXJjaE5vcm1hbCB7XG4gIGVtcGxveWVlTnVtYmVyOiBudW1iZXI7XG4gIGZpcnN0TmFtZTogc3RyaW5nO1xuICBpc0FjdGl2ZTogbnVtYmVyO1xuICBsYW5ndWFnZTogc3RyaW5nO1xuICBsYXN0TmFtZTogc3RyaW5nO1xuICByb2xlczogQXJyYXk8YW55PjtcbiAgdGl0bGU6IHN0cmluZztcbiAgdXNlckFjY2Vzc0dyb3VwczogQXJyYXk8YW55PjtcbiAgdXNlcklkOiBudW1iZXI7XG4gIHVzZXJOYW1lOiBzdHJpbmc7XG59XG5cbi8qKlxuICogIHByb3ZpZGVkSW46ICdyb290JywgfSkgZXhwb3J0IGNsYXNzIFVzZXJTZXJ2aWNlIHsgfSAncm9vdCcgbWVhbnMgdGhhdCB3ZSB3YW50IHByb3ZpZGUgdGhlIHNlcnZpY2UgYXQgdGhlIHJvb3QgbGV2ZWwgKEFwcE1vZHVsZSlcbiAqICBXaGVuIHlvdSBwcm92aWRlIHRoZSBzZXJ2aWNlIGF0IHRoZSByb290IGxldmVsLCBBbmd1bGFyIGNyZWF0ZXMgYSBzaW5nbGUsIHNoYXJlZCBpbnN0YW5jZSBvZiBzZXJ2aWNlIGFuZCBpbmplY3RzIGludG8gYW55IGNsYXNzXG4gKiAgdGhhdCBhc2tzIGZvciBpdC5cbiAqL1xuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xuICB1c2VyU2VhcmNoQ29tcGxleFVybCA9ICcnO1xuICB1c2VyU2VhcmNoTm9ybWFsVXJsID0gJyc7XG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XG4gIGdldENvbXBsZXhTZWFyY2hlZFVzZXJzKCkge1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHRoaXMudXNlclNlYXJjaENvbXBsZXhVcmwpIGFzIE9ic2VydmFibGU8VXNlclNlYXJjaENvbXBsZXhbXT47XG4gIH1cbiAgZ2V0Tm9ybWFsU2VhcmNoZWRVc2VycygpIHtcbiAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodGhpcy51c2VyU2VhcmNoTm9ybWFsVXJsKSBhcyBPYnNlcnZhYmxlPFVzZXJTZWFyY2hOb3JtYWxbXT47XG4gIH1cblxuICBzZXRGYXZvcml0ZUFwcCh1cmksIHBhcmFtT2JqKSB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHVyaSwgcGFyYW1PYmopO1xuICB9XG5cbn1cbiJdfQ==