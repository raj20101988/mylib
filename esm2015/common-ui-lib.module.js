/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoDataAvailableComponent } from './no-data-available/no-data-available.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialComponentModule } from './material-component/material-component.module';
import { FilterComponent } from './filter/filter.component';
import { PaginationComponent } from './pagination/pagination.component';
import { AppSwitcherComponent } from './app-switcher/app-switcher.component';
import { AppMenuComponent } from './app-switcher/app-menu.component';
import { AppListComponent } from './app-switcher/app-list.component';
import { AlertService } from './services/alert.service';
import { UserService } from './services/user.service';
import { SortingService } from './sorting/sorting.service';
import { CommonTranslationModule } from './common-translation/common-translation.module';
import { JwtModule } from '@auth0/angular-jwt';
import { LoaderComponent } from './loader/loader.component';
import { TopNavBarComponent } from "./header/top-nav-bar/top-nav-bar.component";
import { TopologyTreeModule } from './modules/topology-tree.module';
export class CommonUiLibModule {
}
CommonUiLibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    NoDataAvailableComponent,
                    FilterComponent,
                    AppSwitcherComponent,
                    AppMenuComponent,
                    AppListComponent,
                    PaginationComponent,
                    LoaderComponent,
                    TopNavBarComponent
                ],
                imports: [
                    CommonModule,
                    MaterialComponentModule,
                    CommonTranslationModule,
                    TopologyTreeModule,
                    JwtModule
                ],
                providers: [
                    AlertService,
                    UserService,
                    SortingService
                ],
                entryComponents: [],
                exports: [
                    MaterialComponentModule,
                    CommonTranslationModule,
                    HttpClientModule,
                    TopologyTreeModule,
                    NoDataAvailableComponent,
                    FilterComponent,
                    AppSwitcherComponent,
                    AppMenuComponent,
                    PaginationComponent,
                    AppListComponent,
                    JwtModule,
                    LoaderComponent,
                    TopNavBarComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLXVpLWxpYi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJjb21tb24tdWktbGliLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN2QyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLHdCQUF3QixFQUFDLE1BQU0saURBQWlELENBQUM7QUFDekYsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sc0JBQXNCLENBQUM7QUFDdEQsT0FBTyxFQUFDLHVCQUF1QixFQUFDLE1BQU0sZ0RBQWdELENBQUM7QUFDdkYsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQzFELE9BQU8sRUFBQyxtQkFBbUIsRUFBQyxNQUFNLG1DQUFtQyxDQUFDO0FBQ3RFLE9BQU8sRUFBQyxvQkFBb0IsRUFBQyxNQUFNLHVDQUF1QyxDQUFDO0FBQzNFLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLG1DQUFtQyxDQUFDO0FBQ25FLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLG1DQUFtQyxDQUFDO0FBQ25FLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSwwQkFBMEIsQ0FBQztBQUN0RCxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFDcEQsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBQyx1QkFBdUIsRUFBQyxNQUFNLGdEQUFnRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBQyxTQUFTLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUM3QyxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0sMkJBQTJCLENBQUM7QUFDMUQsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sNENBQTRDLENBQUM7QUFDOUUsT0FBTyxFQUFDLGtCQUFrQixFQUFDLE1BQU0sZ0NBQWdDLENBQUM7QUEyQ2xFLE1BQU0sT0FBTyxpQkFBaUI7OztZQXhDN0IsUUFBUSxTQUFDO2dCQUNOLFlBQVksRUFBRTtvQkFDVix3QkFBd0I7b0JBQ3hCLGVBQWU7b0JBQ2Ysb0JBQW9CO29CQUNwQixnQkFBZ0I7b0JBQ2hCLGdCQUFnQjtvQkFDaEIsbUJBQW1CO29CQUNuQixlQUFlO29CQUNmLGtCQUFrQjtpQkFDckI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNMLFlBQVk7b0JBQ1osdUJBQXVCO29CQUN2Qix1QkFBdUI7b0JBQ3ZCLGtCQUFrQjtvQkFDbEIsU0FBUztpQkFDWjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixXQUFXO29CQUNYLGNBQWM7aUJBQ2pCO2dCQUNELGVBQWUsRUFBRSxFQUFFO2dCQUNuQixPQUFPLEVBQUU7b0JBQ0wsdUJBQXVCO29CQUN2Qix1QkFBdUI7b0JBQ3ZCLGdCQUFnQjtvQkFDaEIsa0JBQWtCO29CQUNsQix3QkFBd0I7b0JBQ3hCLGVBQWU7b0JBQ2Ysb0JBQW9CO29CQUNwQixnQkFBZ0I7b0JBQ2hCLG1CQUFtQjtvQkFDbkIsZ0JBQWdCO29CQUNoQixTQUFTO29CQUNULGVBQWU7b0JBQ2Ysa0JBQWtCO2lCQUNyQjthQUNKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Tm9EYXRhQXZhaWxhYmxlQ29tcG9uZW50fSBmcm9tICcuL25vLWRhdGEtYXZhaWxhYmxlL25vLWRhdGEtYXZhaWxhYmxlLmNvbXBvbmVudCc7XG5pbXBvcnQge0h0dHBDbGllbnRNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7TWF0ZXJpYWxDb21wb25lbnRNb2R1bGV9IGZyb20gJy4vbWF0ZXJpYWwtY29tcG9uZW50L21hdGVyaWFsLWNvbXBvbmVudC5tb2R1bGUnO1xuaW1wb3J0IHtGaWx0ZXJDb21wb25lbnR9IGZyb20gJy4vZmlsdGVyL2ZpbHRlci5jb21wb25lbnQnO1xuaW1wb3J0IHtQYWdpbmF0aW9uQ29tcG9uZW50fSBmcm9tICcuL3BhZ2luYXRpb24vcGFnaW5hdGlvbi5jb21wb25lbnQnO1xuaW1wb3J0IHtBcHBTd2l0Y2hlckNvbXBvbmVudH0gZnJvbSAnLi9hcHAtc3dpdGNoZXIvYXBwLXN3aXRjaGVyLmNvbXBvbmVudCc7XG5pbXBvcnQge0FwcE1lbnVDb21wb25lbnR9IGZyb20gJy4vYXBwLXN3aXRjaGVyL2FwcC1tZW51LmNvbXBvbmVudCc7XG5pbXBvcnQge0FwcExpc3RDb21wb25lbnR9IGZyb20gJy4vYXBwLXN3aXRjaGVyL2FwcC1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQge0FsZXJ0U2VydmljZX0gZnJvbSAnLi9zZXJ2aWNlcy9hbGVydC5zZXJ2aWNlJztcbmltcG9ydCB7VXNlclNlcnZpY2V9IGZyb20gJy4vc2VydmljZXMvdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7U29ydGluZ1NlcnZpY2V9IGZyb20gJy4vc29ydGluZy9zb3J0aW5nLnNlcnZpY2UnO1xuaW1wb3J0IHtDb21tb25UcmFuc2xhdGlvbk1vZHVsZX0gZnJvbSAnLi9jb21tb24tdHJhbnNsYXRpb24vY29tbW9uLXRyYW5zbGF0aW9uLm1vZHVsZSc7XG5pbXBvcnQge0p3dE1vZHVsZX0gZnJvbSAnQGF1dGgwL2FuZ3VsYXItand0JztcbmltcG9ydCB7TG9hZGVyQ29tcG9uZW50fSBmcm9tICcuL2xvYWRlci9sb2FkZXIuY29tcG9uZW50JztcbmltcG9ydCB7VG9wTmF2QmFyQ29tcG9uZW50fSBmcm9tIFwiLi9oZWFkZXIvdG9wLW5hdi1iYXIvdG9wLW5hdi1iYXIuY29tcG9uZW50XCI7XG5pbXBvcnQge1RvcG9sb2d5VHJlZU1vZHVsZX0gZnJvbSAnLi9tb2R1bGVzL3RvcG9sb2d5LXRyZWUubW9kdWxlJztcblxuXG5ATmdNb2R1bGUoe1xuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBOb0RhdGFBdmFpbGFibGVDb21wb25lbnQsXG4gICAgICAgIEZpbHRlckNvbXBvbmVudCxcbiAgICAgICAgQXBwU3dpdGNoZXJDb21wb25lbnQsXG4gICAgICAgIEFwcE1lbnVDb21wb25lbnQsXG4gICAgICAgIEFwcExpc3RDb21wb25lbnQsXG4gICAgICAgIFBhZ2luYXRpb25Db21wb25lbnQsXG4gICAgICAgIExvYWRlckNvbXBvbmVudCxcbiAgICAgICAgVG9wTmF2QmFyQ29tcG9uZW50XG4gICAgXSxcbiAgICBpbXBvcnRzOiBbXG4gICAgICAgIENvbW1vbk1vZHVsZSxcbiAgICAgICAgTWF0ZXJpYWxDb21wb25lbnRNb2R1bGUsXG4gICAgICAgIENvbW1vblRyYW5zbGF0aW9uTW9kdWxlLFxuICAgICAgICBUb3BvbG9neVRyZWVNb2R1bGUsXG4gICAgICAgIEp3dE1vZHVsZVxuICAgIF0sXG4gICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIEFsZXJ0U2VydmljZSxcbiAgICAgICAgVXNlclNlcnZpY2UsXG4gICAgICAgIFNvcnRpbmdTZXJ2aWNlXG4gICAgXSxcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtdLFxuICAgIGV4cG9ydHM6IFtcbiAgICAgICAgTWF0ZXJpYWxDb21wb25lbnRNb2R1bGUsXG4gICAgICAgIENvbW1vblRyYW5zbGF0aW9uTW9kdWxlLFxuICAgICAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgICAgICBUb3BvbG9neVRyZWVNb2R1bGUsXG4gICAgICAgIE5vRGF0YUF2YWlsYWJsZUNvbXBvbmVudCxcbiAgICAgICAgRmlsdGVyQ29tcG9uZW50LFxuICAgICAgICBBcHBTd2l0Y2hlckNvbXBvbmVudCxcbiAgICAgICAgQXBwTWVudUNvbXBvbmVudCxcbiAgICAgICAgUGFnaW5hdGlvbkNvbXBvbmVudCxcbiAgICAgICAgQXBwTGlzdENvbXBvbmVudCxcbiAgICAgICAgSnd0TW9kdWxlLFxuICAgICAgICBMb2FkZXJDb21wb25lbnQsXG4gICAgICAgIFRvcE5hdkJhckNvbXBvbmVudFxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgQ29tbW9uVWlMaWJNb2R1bGUge1xufVxuIl19