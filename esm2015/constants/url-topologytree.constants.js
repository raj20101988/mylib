/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { protocol, webServerDNS } from './url.common.constants';
/**
 * @record
 */
export function UrlTopologyTreeConstantsInterface() { }
if (false) {
    /** @type {?} */
    UrlTopologyTreeConstantsInterface.prototype.topologyTreeNodesUrl;
    /** @type {?} */
    UrlTopologyTreeConstantsInterface.prototype.unassignedNondeUrl;
    /** @type {?} */
    UrlTopologyTreeConstantsInterface.prototype.topologyNodesUrls;
    /** @type {?} */
    UrlTopologyTreeConstantsInterface.prototype.accessGroupNodesUrl;
}
/** @type {?} */
const basePath = '/api/topology';
/** @type {?} */
const version = '/v1/';
/** @type {?} */
const commonPath = 'topologyNodes/';
/** @type {?} */
const topologyGroupPath = 'topologyGroups/';
/** @type {?} */
const baseUrl = protocol + webServerDNS + basePath + version;
/** @type {?} */
export const topologyTree = {
    topologyNodesUrls: baseUrl + commonPath,
    topologyTreeNodesUrl: baseUrl + commonPath + '?isCache=false',
    unassignedNondeUrl: baseUrl + commonPath + 'global/UNASSIGNED',
    accessGroupNodesUrl: baseUrl + topologyGroupPath
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLXRvcG9sb2d5dHJlZS5jb25zdGFudHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJjb25zdGFudHMvdXJsLXRvcG9sb2d5dHJlZS5jb25zdGFudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxRQUFRLEVBQUUsWUFBWSxFQUFDLE1BQU0sd0JBQXdCLENBQUM7Ozs7QUFFOUQsdURBS0M7OztJQUpHLGlFQUE2Qjs7SUFDN0IsK0RBQTJCOztJQUMzQiw4REFBMEI7O0lBQzFCLGdFQUE0Qjs7O01BRzFCLFFBQVEsR0FBRyxlQUFlOztNQUMxQixPQUFPLEdBQUcsTUFBTTs7TUFDaEIsVUFBVSxHQUFHLGdCQUFnQjs7TUFDN0IsaUJBQWlCLEdBQUcsaUJBQWlCOztNQUNyQyxPQUFPLEdBQUcsUUFBUSxHQUFHLFlBQVksR0FBRyxRQUFRLEdBQUcsT0FBTzs7QUFFNUQsTUFBTSxPQUFPLFlBQVksR0FBc0M7SUFDM0QsaUJBQWlCLEVBQUUsT0FBTyxHQUFHLFVBQVU7SUFDdkMsb0JBQW9CLEVBQUUsT0FBTyxHQUFHLFVBQVUsR0FBRyxnQkFBZ0I7SUFDN0Qsa0JBQWtCLEVBQUUsT0FBTyxHQUFHLFVBQVUsR0FBRyxtQkFBbUI7SUFDOUQsbUJBQW1CLEVBQUUsT0FBTyxHQUFHLGlCQUFpQjtDQUNuRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7cHJvdG9jb2wsIHdlYlNlcnZlckROU30gZnJvbSAnLi91cmwuY29tbW9uLmNvbnN0YW50cyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgVXJsVG9wb2xvZ3lUcmVlQ29uc3RhbnRzSW50ZXJmYWNlIHtcbiAgICB0b3BvbG9neVRyZWVOb2Rlc1VybDogc3RyaW5nO1xuICAgIHVuYXNzaWduZWROb25kZVVybDogc3RyaW5nO1xuICAgIHRvcG9sb2d5Tm9kZXNVcmxzOiBzdHJpbmc7XG4gICAgYWNjZXNzR3JvdXBOb2Rlc1VybDogc3RyaW5nO1xufVxuXG5jb25zdCBiYXNlUGF0aCA9ICcvYXBpL3RvcG9sb2d5JztcbmNvbnN0IHZlcnNpb24gPSAnL3YxLyc7XG5jb25zdCBjb21tb25QYXRoID0gJ3RvcG9sb2d5Tm9kZXMvJztcbmNvbnN0IHRvcG9sb2d5R3JvdXBQYXRoID0gJ3RvcG9sb2d5R3JvdXBzLydcbmNvbnN0IGJhc2VVcmwgPSBwcm90b2NvbCArIHdlYlNlcnZlckROUyArIGJhc2VQYXRoICsgdmVyc2lvbjtcblxuZXhwb3J0IGNvbnN0IHRvcG9sb2d5VHJlZTogVXJsVG9wb2xvZ3lUcmVlQ29uc3RhbnRzSW50ZXJmYWNlID0ge1xuICAgIHRvcG9sb2d5Tm9kZXNVcmxzOiBiYXNlVXJsICsgY29tbW9uUGF0aCxcbiAgICB0b3BvbG9neVRyZWVOb2Rlc1VybDogYmFzZVVybCArIGNvbW1vblBhdGggKyAnP2lzQ2FjaGU9ZmFsc2UnLFxuICAgIHVuYXNzaWduZWROb25kZVVybDogYmFzZVVybCArIGNvbW1vblBhdGggKyAnZ2xvYmFsL1VOQVNTSUdORUQnLFxuICAgIGFjY2Vzc0dyb3VwTm9kZXNVcmw6IGJhc2VVcmwgKyB0b3BvbG9neUdyb3VwUGF0aFxufTtcblxuIl19