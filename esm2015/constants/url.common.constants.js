/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/** @type {?} */
export const protocol = 'https://';
/** @type {?} */
export const authenticationType = 'LDAP';
/** @type {?} */
export const webServerDNS = 'wdts-gateway-dev7.wdtablesystems.com';
/** @type {?} */
export const treasuryPort = '#@webTreasuryTLSPort@#';
/** @type {?} */
export const webDashboardPort = '#@webDashboardTLSPort@#';
/** @type {?} */
export const webConfigurationTLSPort = '#@webConfigurationTLSPort@#';
/** @type {?} */
export const webCasinoManagerTLSPort = '#@webCasinoManagerTLSPort@#';
/** @type {?} */
export const webPlayerDashboardTLSPort = '#@webPlayerDashboardTLSPort@#';
/** @type {?} */
export const webAlertsTLSPort = '#@webAlertsTLSPort@#';
/** @type {?} */
export const webTableDashboardTLSPort = '#@webTableDashboardTLSPort@#';
/** @type {?} */
export const webCamTLSPort = '#@webCamTLSPort@#';
/** @type {?} */
export const webCashierTLSPort = '#@webCashierTLSPort@#';
/** @type {?} */
export const webLoginPort = '#@webLoginTLSPort@#';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLmNvbW1vbi5jb25zdGFudHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJjb25zdGFudHMvdXJsLmNvbW1vbi5jb25zdGFudHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxNQUFNLE9BQU8sUUFBUSxHQUFHLFVBQVU7O0FBQ2xDLE1BQU0sT0FBTyxrQkFBa0IsR0FBRyxNQUFNOztBQUN4QyxNQUFNLE9BQU8sWUFBWSxHQUFHLHNDQUFzQzs7QUFDbEUsTUFBTSxPQUFPLFlBQVksR0FBRyx3QkFBd0I7O0FBQ3BELE1BQU0sT0FBTyxnQkFBZ0IsR0FBRyx5QkFBeUI7O0FBQ3pELE1BQU0sT0FBTyx1QkFBdUIsR0FBRyw2QkFBNkI7O0FBQ3BFLE1BQU0sT0FBTyx1QkFBdUIsR0FBRyw2QkFBNkI7O0FBQ3BFLE1BQU0sT0FBTyx5QkFBeUIsR0FBRywrQkFBK0I7O0FBQ3hFLE1BQU0sT0FBTyxnQkFBZ0IsR0FBRyxzQkFBc0I7O0FBQ3RELE1BQU0sT0FBTyx3QkFBd0IsR0FBRyw4QkFBOEI7O0FBQ3RFLE1BQU0sT0FBTyxhQUFhLEdBQUcsbUJBQW1COztBQUNoRCxNQUFNLE9BQU8saUJBQWlCLEdBQUcsdUJBQXVCOztBQUN4RCxNQUFNLE9BQU8sWUFBWSxHQUFHLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBwcm90b2NvbCA9ICdodHRwczovLyc7XG5leHBvcnQgY29uc3QgYXV0aGVudGljYXRpb25UeXBlID0gJ0xEQVAnO1xuZXhwb3J0IGNvbnN0IHdlYlNlcnZlckROUyA9ICd3ZHRzLWdhdGV3YXktZGV2Ny53ZHRhYmxlc3lzdGVtcy5jb20nO1xuZXhwb3J0IGNvbnN0IHRyZWFzdXJ5UG9ydCA9ICcjQHdlYlRyZWFzdXJ5VExTUG9ydEAjJztcbmV4cG9ydCBjb25zdCB3ZWJEYXNoYm9hcmRQb3J0ID0gJyNAd2ViRGFzaGJvYXJkVExTUG9ydEAjJztcbmV4cG9ydCBjb25zdCB3ZWJDb25maWd1cmF0aW9uVExTUG9ydCA9ICcjQHdlYkNvbmZpZ3VyYXRpb25UTFNQb3J0QCMnO1xuZXhwb3J0IGNvbnN0IHdlYkNhc2lub01hbmFnZXJUTFNQb3J0ID0gJyNAd2ViQ2FzaW5vTWFuYWdlclRMU1BvcnRAIyc7XG5leHBvcnQgY29uc3Qgd2ViUGxheWVyRGFzaGJvYXJkVExTUG9ydCA9ICcjQHdlYlBsYXllckRhc2hib2FyZFRMU1BvcnRAIyc7XG5leHBvcnQgY29uc3Qgd2ViQWxlcnRzVExTUG9ydCA9ICcjQHdlYkFsZXJ0c1RMU1BvcnRAIyc7XG5leHBvcnQgY29uc3Qgd2ViVGFibGVEYXNoYm9hcmRUTFNQb3J0ID0gJyNAd2ViVGFibGVEYXNoYm9hcmRUTFNQb3J0QCMnO1xuZXhwb3J0IGNvbnN0IHdlYkNhbVRMU1BvcnQgPSAnI0B3ZWJDYW1UTFNQb3J0QCMnO1xuZXhwb3J0IGNvbnN0IHdlYkNhc2hpZXJUTFNQb3J0ID0gJyNAd2ViQ2FzaGllclRMU1BvcnRAIyc7XG5leHBvcnQgY29uc3Qgd2ViTG9naW5Qb3J0ID0gJyNAd2ViTG9naW5UTFNQb3J0QCMnO1xuIl19