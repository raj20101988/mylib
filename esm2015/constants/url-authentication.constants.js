/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { protocol, webServerDNS } from './url.common.constants';
/**
 * @record
 */
export function UrlAuthenticationInterface() { }
if (false) {
    /** @type {?} */
    UrlAuthenticationInterface.prototype.login;
    /** @type {?} */
    UrlAuthenticationInterface.prototype.refresh;
}
/** @type {?} */
const basePath = '/api/auth';
/** @type {?} */
const oAuth = '/oauth/token';
/** @type {?} */
const baseUrl = protocol + webServerDNS + basePath + oAuth;
/** @type {?} */
export const auth = {
    login: baseUrl + '/login',
    refresh: baseUrl + '/refresh'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLWF1dGhlbnRpY2F0aW9uLmNvbnN0YW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbImNvbnN0YW50cy91cmwtYXV0aGVudGljYXRpb24uY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFFLFlBQVksRUFBQyxNQUFNLHdCQUF3QixDQUFDOzs7O0FBRTlELGdEQUdDOzs7SUFGRywyQ0FBYzs7SUFDZCw2Q0FBZ0I7OztNQUdkLFFBQVEsR0FBRyxXQUFXOztNQUN0QixLQUFLLEdBQUcsY0FBYzs7TUFDdEIsT0FBTyxHQUFHLFFBQVEsR0FBRyxZQUFZLEdBQUcsUUFBUSxHQUFHLEtBQUs7O0FBRTFELE1BQU0sT0FBTyxJQUFJLEdBQStCO0lBQzVDLEtBQUssRUFBRSxPQUFPLEdBQUcsUUFBUTtJQUN6QixPQUFPLEVBQUUsT0FBTyxHQUFHLFVBQVU7Q0FDaEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge3Byb3RvY29sLCB3ZWJTZXJ2ZXJETlN9IGZyb20gJy4vdXJsLmNvbW1vbi5jb25zdGFudHMnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFVybEF1dGhlbnRpY2F0aW9uSW50ZXJmYWNlIHtcbiAgICBsb2dpbjogc3RyaW5nO1xuICAgIHJlZnJlc2g6IHN0cmluZztcbn1cblxuY29uc3QgYmFzZVBhdGggPSAnL2FwaS9hdXRoJztcbmNvbnN0IG9BdXRoID0gJy9vYXV0aC90b2tlbic7XG5jb25zdCBiYXNlVXJsID0gcHJvdG9jb2wgKyB3ZWJTZXJ2ZXJETlMgKyBiYXNlUGF0aCArIG9BdXRoO1xuXG5leHBvcnQgY29uc3QgYXV0aDogVXJsQXV0aGVudGljYXRpb25JbnRlcmZhY2UgPSB7XG4gICAgbG9naW46IGJhc2VVcmwgKyAnL2xvZ2luJyxcbiAgICByZWZyZXNoOiBiYXNlVXJsICsgJy9yZWZyZXNoJ1xufTtcblxuIl19