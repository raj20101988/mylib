/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { alert } from './url-alert.constants';
import { auth } from './url-authentication.constants';
import { user } from './url-user.constants';
import { configuration } from './url-configuration.constants';
import { topologyTree } from './url-topologytree.constants';
/**
 * @record
 */
export function URLInterface() { }
if (false) {
    /** @type {?} */
    URLInterface.prototype.alert;
    /** @type {?} */
    URLInterface.prototype.auth;
    /** @type {?} */
    URLInterface.prototype.user;
    /** @type {?} */
    URLInterface.prototype.config;
    /** @type {?} */
    URLInterface.prototype.topologyTree;
}
/** @type {?} */
export const urls = {
    alert: alert,
    auth: auth,
    user: user,
    config: configuration,
    topologyTree: topologyTree
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJscy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbImNvbnN0YW50cy91cmxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsS0FBSyxFQUE2QixNQUFNLHVCQUF1QixDQUFDO0FBQ3hFLE9BQU8sRUFBQyxJQUFJLEVBQTZCLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEYsT0FBTyxFQUE0QixJQUFJLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUNyRSxPQUFPLEVBQUMsYUFBYSxFQUFxQyxNQUFNLCtCQUErQixDQUFDO0FBQ2hHLE9BQU8sRUFBb0MsWUFBWSxFQUFDLE1BQU0sOEJBQThCLENBQUM7Ozs7QUFHN0Ysa0NBTUM7OztJQUxHLDZCQUFrQzs7SUFDbEMsNEJBQWlDOztJQUNqQyw0QkFBZ0M7O0lBQ2hDLDhCQUEyQzs7SUFDM0Msb0NBQWdEOzs7QUFHcEQsTUFBTSxPQUFPLElBQUksR0FBaUI7SUFDOUIsS0FBSyxFQUFFLEtBQUs7SUFDWixJQUFJLEVBQUUsSUFBSTtJQUNWLElBQUksRUFBRSxJQUFJO0lBQ1YsTUFBTSxFQUFFLGFBQWE7SUFDckIsWUFBWSxFQUFFLFlBQVk7Q0FDN0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2FsZXJ0LCBVcmxBbGVydENvbnN0YW50c0ludGVyZmFjZX0gZnJvbSAnLi91cmwtYWxlcnQuY29uc3RhbnRzJztcbmltcG9ydCB7YXV0aCwgVXJsQXV0aGVudGljYXRpb25JbnRlcmZhY2V9IGZyb20gJy4vdXJsLWF1dGhlbnRpY2F0aW9uLmNvbnN0YW50cyc7XG5pbXBvcnQge1VybFVzZXJDb25zdGFudHNJbnRlcmZhY2UsIHVzZXJ9IGZyb20gJy4vdXJsLXVzZXIuY29uc3RhbnRzJztcbmltcG9ydCB7Y29uZmlndXJhdGlvbiwgVXJsQ29uZmlndXJhdGlvbkNvbnN0YW50c0ludGVyZmFjZX0gZnJvbSAnLi91cmwtY29uZmlndXJhdGlvbi5jb25zdGFudHMnO1xuaW1wb3J0IHtVcmxUb3BvbG9neVRyZWVDb25zdGFudHNJbnRlcmZhY2UsIHRvcG9sb2d5VHJlZX0gZnJvbSAnLi91cmwtdG9wb2xvZ3l0cmVlLmNvbnN0YW50cyc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBVUkxJbnRlcmZhY2Uge1xuICAgIGFsZXJ0OiBVcmxBbGVydENvbnN0YW50c0ludGVyZmFjZTtcbiAgICBhdXRoOiBVcmxBdXRoZW50aWNhdGlvbkludGVyZmFjZTtcbiAgICB1c2VyOiBVcmxVc2VyQ29uc3RhbnRzSW50ZXJmYWNlO1xuICAgIGNvbmZpZzogVXJsQ29uZmlndXJhdGlvbkNvbnN0YW50c0ludGVyZmFjZTtcbiAgICB0b3BvbG9neVRyZWU6IFVybFRvcG9sb2d5VHJlZUNvbnN0YW50c0ludGVyZmFjZTtcbn1cblxuZXhwb3J0IGNvbnN0IHVybHM6IFVSTEludGVyZmFjZSA9IHtcbiAgICBhbGVydDogYWxlcnQsXG4gICAgYXV0aDogYXV0aCxcbiAgICB1c2VyOiB1c2VyLFxuICAgIGNvbmZpZzogY29uZmlndXJhdGlvbixcbiAgICB0b3BvbG9neVRyZWU6IHRvcG9sb2d5VHJlZVxufTtcblxuXG4iXX0=