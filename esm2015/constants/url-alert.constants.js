/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { protocol, webServerDNS } from './url.common.constants';
/**
 * @record
 */
export function UrlAlertConstantsInterface() { }
if (false) {
    /** @type {?} */
    UrlAlertConstantsInterface.prototype.socketAlert;
    /** @type {?} */
    UrlAlertConstantsInterface.prototype.alertsPath;
    /** @type {?} */
    UrlAlertConstantsInterface.prototype.alertCountsPath;
    /** @type {?} */
    UrlAlertConstantsInterface.prototype.alertConfigurationPath;
    /** @type {?} */
    UrlAlertConstantsInterface.prototype.metricsPath;
    /** @type {?} */
    UrlAlertConstantsInterface.prototype.filter;
}
/** @type {?} */
const basePath = '/api/alert';
/** @type {?} */
const version = '/v1/';
/** @type {?} */
const baseUrl = protocol + webServerDNS + basePath + version;
/** @type {?} */
export const alert = {
    socketAlert: protocol + webServerDNS + '/alert',
    alertsPath: baseUrl + 'alerts',
    alertCountsPath: baseUrl + 'alertCounts',
    alertConfigurationPath: baseUrl + 'alertConfiguration',
    metricsPath: baseUrl + 'metrics',
    filter: baseUrl + 'filters'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLWFsZXJ0LmNvbnN0YW50cy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbImNvbnN0YW50cy91cmwtYWxlcnQuY29uc3RhbnRzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFFLFlBQVksRUFBQyxNQUFNLHdCQUF3QixDQUFDOzs7O0FBRTlELGdEQU9DOzs7SUFOQyxpREFBb0I7O0lBQ3BCLGdEQUFtQjs7SUFDbkIscURBQXdCOztJQUN4Qiw0REFBK0I7O0lBQy9CLGlEQUFvQjs7SUFDcEIsNENBQWU7OztNQUdYLFFBQVEsR0FBRyxZQUFZOztNQUN2QixPQUFPLEdBQUcsTUFBTTs7TUFDaEIsT0FBTyxHQUFHLFFBQVEsR0FBRyxZQUFZLEdBQUcsUUFBUSxHQUFHLE9BQU87O0FBRTVELE1BQU0sT0FBTyxLQUFLLEdBQStCO0lBQy9DLFdBQVcsRUFBRSxRQUFRLEdBQUcsWUFBWSxHQUFHLFFBQVE7SUFDL0MsVUFBVSxFQUFFLE9BQU8sR0FBRyxRQUFRO0lBQzlCLGVBQWUsRUFBRSxPQUFPLEdBQUcsYUFBYTtJQUN4QyxzQkFBc0IsRUFBRSxPQUFPLEdBQUcsb0JBQW9CO0lBQ3RELFdBQVcsRUFBRSxPQUFPLEdBQUcsU0FBUztJQUNoQyxNQUFNLEVBQUUsT0FBTyxHQUFHLFNBQVM7Q0FDNUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge3Byb3RvY29sLCB3ZWJTZXJ2ZXJETlN9IGZyb20gJy4vdXJsLmNvbW1vbi5jb25zdGFudHMnO1xuXG5leHBvcnQgaW50ZXJmYWNlIFVybEFsZXJ0Q29uc3RhbnRzSW50ZXJmYWNlIHtcbiAgc29ja2V0QWxlcnQ6IFN0cmluZztcbiAgYWxlcnRzUGF0aDogU3RyaW5nO1xuICBhbGVydENvdW50c1BhdGg6IFN0cmluZztcbiAgYWxlcnRDb25maWd1cmF0aW9uUGF0aDogU3RyaW5nO1xuICBtZXRyaWNzUGF0aDogU3RyaW5nO1xuICBmaWx0ZXI6IFN0cmluZztcbn1cblxuY29uc3QgYmFzZVBhdGggPSAnL2FwaS9hbGVydCc7XG5jb25zdCB2ZXJzaW9uID0gJy92MS8nO1xuY29uc3QgYmFzZVVybCA9IHByb3RvY29sICsgd2ViU2VydmVyRE5TICsgYmFzZVBhdGggKyB2ZXJzaW9uO1xuXG5leHBvcnQgY29uc3QgYWxlcnQ6IFVybEFsZXJ0Q29uc3RhbnRzSW50ZXJmYWNlID0ge1xuICBzb2NrZXRBbGVydDogcHJvdG9jb2wgKyB3ZWJTZXJ2ZXJETlMgKyAnL2FsZXJ0JyxcbiAgYWxlcnRzUGF0aDogYmFzZVVybCArICdhbGVydHMnLFxuICBhbGVydENvdW50c1BhdGg6IGJhc2VVcmwgKyAnYWxlcnRDb3VudHMnLFxuICBhbGVydENvbmZpZ3VyYXRpb25QYXRoOiBiYXNlVXJsICsgJ2FsZXJ0Q29uZmlndXJhdGlvbicsXG4gIG1ldHJpY3NQYXRoOiBiYXNlVXJsICsgJ21ldHJpY3MnLFxuICBmaWx0ZXI6IGJhc2VVcmwgKyAnZmlsdGVycydcbn07XG5cbiJdfQ==