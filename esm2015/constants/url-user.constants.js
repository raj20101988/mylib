/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { protocol, webServerDNS } from './url.common.constants';
/**
 * @record
 */
export function UrlUserConstantsInterface() { }
if (false) {
    /** @type {?} */
    UrlUserConstantsInterface.prototype.users;
}
/** @type {?} */
const basePath = '/api/user';
/** @type {?} */
const version = '/v1/';
/** @type {?} */
const baseUrl = protocol + webServerDNS + basePath + version;
/** @type {?} */
export const user = {
    users: baseUrl + 'users/1'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLXVzZXIuY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsiY29uc3RhbnRzL3VybC11c2VyLmNvbnN0YW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQzs7OztBQUU5RCwrQ0FFQzs7O0lBREcsMENBQWM7OztNQUdaLFFBQVEsR0FBRyxXQUFXOztNQUN0QixPQUFPLEdBQUcsTUFBTTs7TUFDaEIsT0FBTyxHQUFHLFFBQVEsR0FBRyxZQUFZLEdBQUcsUUFBUSxHQUFHLE9BQU87O0FBRTVELE1BQU0sT0FBTyxJQUFJLEdBQThCO0lBQzNDLEtBQUssRUFBRSxPQUFPLEdBQUcsU0FBUztDQUM3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7cHJvdG9jb2wsIHdlYlNlcnZlckROU30gZnJvbSAnLi91cmwuY29tbW9uLmNvbnN0YW50cyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgVXJsVXNlckNvbnN0YW50c0ludGVyZmFjZSB7XG4gICAgdXNlcnM6IFN0cmluZztcbn1cblxuY29uc3QgYmFzZVBhdGggPSAnL2FwaS91c2VyJztcbmNvbnN0IHZlcnNpb24gPSAnL3YxLyc7XG5jb25zdCBiYXNlVXJsID0gcHJvdG9jb2wgKyB3ZWJTZXJ2ZXJETlMgKyBiYXNlUGF0aCArIHZlcnNpb247XG5cbmV4cG9ydCBjb25zdCB1c2VyOiBVcmxVc2VyQ29uc3RhbnRzSW50ZXJmYWNlID0ge1xuICAgIHVzZXJzOiBiYXNlVXJsICsgJ3VzZXJzLzEnXG59O1xuXG4iXX0=