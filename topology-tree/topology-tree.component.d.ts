import { OnInit, QueryList } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject } from 'rxjs';
import { TopologytreeService } from '../services/topologytree.service';
import { CdkDrag } from '@angular/cdk/drag-drop';
export declare class FileNode {
    children: FileNode[];
    filename: string;
    type: any;
}
export declare class FileDatabase {
    dataChangeAssigned: BehaviorSubject<FileNode[]>;
    dataChangeUnassigned: BehaviorSubject<FileNode[]>;
    dataChangeAG: BehaviorSubject<FileNode[]>;
    dataChangeVG: BehaviorSubject<FileNode[]>;
    constructor();
    initialize(): void;
    buildFileTree(obj: any, level: number): FileNode[];
}
export declare class TopologyTreeComponent implements OnInit {
    database: FileDatabase;
    private _topologyTreeService;
    cdkDrag: QueryList<CdkDrag>;
    arrTrees: Array<any>;
    topologyTreeConfig: import("../constants/url-topologytree.constants").UrlTopologyTreeConstantsInterface;
    nestedTreeControl: NestedTreeControl<FileNode>;
    assignedDataSource: MatTreeNestedDataSource<FileNode>;
    unassignedDataSource: MatTreeNestedDataSource<FileNode>;
    accessGroupDataSource: MatTreeNestedDataSource<FileNode>;
    virtualGroupDataSource: MatTreeNestedDataSource<FileNode>;
    arrNestedDataSource: any[];
    constructor(database: FileDatabase, _topologyTreeService: TopologytreeService);
    hasNestedChild: (_: number, nodeData: FileNode) => boolean;
    private _getChildren;
    ngOnInit(): void;
    private _createDataSources;
    private _setUnassignedNodes;
    private _setAccessGroupNodes;
    private _getNestedNodes;
}
