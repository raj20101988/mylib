import { OnInit, EventEmitter } from '@angular/core';
export declare class PaginationComponent implements OnInit {
    perPageOption: Array<number>;
    totalRecord: number;
    EMIT_PAGINATION: EventEmitter<any>;
    start: number;
    limit: number;
    currentPage: number;
    totalPage: number;
    constructor();
    ngOnInit(): void;
    nextPage(): void;
    previousPage(): void;
    firstPage(): void;
    lastPage(): void;
    changePageLimit(limit: any): void;
    goToAnyPage(event: any, currntPage: any): void;
    emitPagination(): void;
}
