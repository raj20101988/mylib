/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { protocol, webServerDNS } from './url.common.constants';
/**
 * @record
 */
export function UrlConfigurationConstantsInterface() { }
if (false) {
    /** @type {?} */
    UrlConfigurationConstantsInterface.prototype.categories;
    /** @type {?} */
    UrlConfigurationConstantsInterface.prototype.configurations;
}
/** @type {?} */
var basePath = '/api/configuration';
/** @type {?} */
var version = '/v1/';
/** @type {?} */
var baseUrl = protocol + webServerDNS + basePath + version;
/** @type {?} */
export var configuration = {
    categories: baseUrl + 'categories',
    configurations: baseUrl + 'configurations'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXJsLWNvbmZpZ3VyYXRpb24uY29uc3RhbnRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsiY29uc3RhbnRzL3VybC1jb25maWd1cmF0aW9uLmNvbnN0YW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQzs7OztBQUU5RCx3REFHQzs7O0lBRkcsd0RBQW1COztJQUNuQiw0REFBdUI7OztJQUdyQixRQUFRLEdBQUcsb0JBQW9COztJQUMvQixPQUFPLEdBQUcsTUFBTTs7SUFDaEIsT0FBTyxHQUFHLFFBQVEsR0FBRyxZQUFZLEdBQUcsUUFBUSxHQUFHLE9BQU87O0FBRTVELE1BQU0sS0FBTyxhQUFhLEdBQXVDO0lBQzdELFVBQVUsRUFBRSxPQUFPLEdBQUcsWUFBWTtJQUNsQyxjQUFjLEVBQUUsT0FBTyxHQUFHLGdCQUFnQjtDQUM3QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7cHJvdG9jb2wsIHdlYlNlcnZlckROU30gZnJvbSAnLi91cmwuY29tbW9uLmNvbnN0YW50cyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgVXJsQ29uZmlndXJhdGlvbkNvbnN0YW50c0ludGVyZmFjZSB7XG4gICAgY2F0ZWdvcmllczogc3RyaW5nO1xuICAgIGNvbmZpZ3VyYXRpb25zOiBzdHJpbmc7XG59XG5cbmNvbnN0IGJhc2VQYXRoID0gJy9hcGkvY29uZmlndXJhdGlvbic7XG5jb25zdCB2ZXJzaW9uID0gJy92MS8nO1xuY29uc3QgYmFzZVVybCA9IHByb3RvY29sICsgd2ViU2VydmVyRE5TICsgYmFzZVBhdGggKyB2ZXJzaW9uO1xuXG5leHBvcnQgY29uc3QgY29uZmlndXJhdGlvbjogVXJsQ29uZmlndXJhdGlvbkNvbnN0YW50c0ludGVyZmFjZSA9IHtcbiAgICBjYXRlZ29yaWVzOiBiYXNlVXJsICsgJ2NhdGVnb3JpZXMnLFxuICAgIGNvbmZpZ3VyYXRpb25zOiBiYXNlVXJsICsgJ2NvbmZpZ3VyYXRpb25zJ1xufTtcblxuIl19