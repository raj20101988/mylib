/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
var FilterComponent = /** @class */ (function () {
    function FilterComponent(translate) {
        this.translate = translate;
        this.EMIT_FILTER = new EventEmitter();
        this.filterConfigOption = {
            title: '',
            column: '',
            class: '',
            options: [],
            selectedOptions: []
        };
        this.options = [];
        this.searchKey = '';
        this.index = -1;
    }
    /**
     * @return {?}
     */
    FilterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.filterConfigOption = this.configOptions;
        for (var i = 0; i < this.filterConfigOption.selectedOptions.length; i++) {
            this.options.push(this.filterConfigOption.selectedOptions[i]);
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    FilterComponent.prototype.selectOption = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        if (params) {
            this.selectDeselectAll(params);
        }
        else {
            this.selectDeselectOtherOptions();
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    FilterComponent.prototype.selectDeselectAll = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        if (this.options.indexOf('All') > -1) {
            /** @type {?} */
            var tmpOption = ['All'];
            for (var i = 0; i < this.filterConfigOption.options.length; i++) {
                tmpOption.push(this.filterConfigOption.options[i]);
            }
            this.options = tmpOption;
        }
        else {
            this.options = [];
        }
        this.emitFilter(params);
    };
    /**
     * @return {?}
     */
    FilterComponent.prototype.selectDeselectOtherOptions = /**
     * @return {?}
     */
    function () {
        if (this.options.length === this.filterConfigOption.options.length && this.options.indexOf('All') < 0) {
            /** @type {?} */
            var tmpOption = ['All'];
            for (var i = 0; i < this.filterConfigOption.options.length; i++) {
                tmpOption.push(this.filterConfigOption.options[i]);
            }
            this.options = tmpOption;
            this.emitFilter(true);
        }
        else if (this.options.length === this.filterConfigOption.options.length) {
            /** @type {?} */
            var tmpOption = Array.from(this.options);
            tmpOption.shift();
            this.options = tmpOption;
            this.emitFilter(false);
        }
        else {
            this.emitFilter(false);
        }
    };
    /**
     * @param {?} params
     * @return {?}
     */
    FilterComponent.prototype.emitFilter = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var excludeAll = Array.from(this.options);
        if (params) {
            excludeAll.shift();
        }
        this.EMIT_FILTER.emit({ columnName: this.filterConfigOption.column, selectedOption: excludeAll });
    };
    /**
     * @param {?} option
     * @return {?}
     */
    FilterComponent.prototype.translateFilterOptions = /**
     * @param {?} option
     * @return {?}
     */
    function (option) {
        /** @type {?} */
        var translateStr = '';
        if (this.filterConfigOption.translatePath && option !== 'All') {
            translateStr = this.filterConfigOption.column === 'eventTypes' ? this.translate.instant(option + '.Alert_Type_Description') :
                this.translate.instant(this.filterConfigOption.translatePath + '.' + option);
        }
        else {
            translateStr = option;
        }
        return translateStr;
    };
    FilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-filter',
                    template: "<mat-form-field class=\"filter-field__container\">\n  <mat-select placeholder=\"{{filterConfigOption.title | translate}}\" [(ngModel)]=\"options\" multiple>\n    <mat-select-trigger>\n      <span *ngIf=\"options.indexOf('All') > -1\" [translate]=\"'All'\"></span>\n      <span *ngIf=\"options.indexOf('All') < 0\">{{options ? translateFilterOptions(options[0]) : ''}}</span>\n      <span *ngIf=\"options?.length > 1 && options.indexOf('All') < 0\" class=\"example-additional-selection\">\n        (+{{options.length - 1}})\n      </span>\n    </mat-select-trigger>\n\n    <mat-option [value]=\"'All'\" (click)=\"selectOption(true)\"><span [translate]=\"'All'\"></span></mat-option>\n\n    <mat-option *ngFor=\"let option of filterConfigOption.options; let idx=index\" id=\"option{{idx}}\" [value]=\"option\" (click)=\"selectOption(false)\">\n      {{translateFilterOptions(option)}}\n    </mat-option>\n  </mat-select>\n</mat-form-field>\n",
                    styles: [".filter-field__container{width:100%}"]
                }] }
    ];
    /** @nocollapse */
    FilterComponent.ctorParameters = function () { return [
        { type: TranslateService }
    ]; };
    FilterComponent.propDecorators = {
        configOptions: [{ type: Input }],
        EMIT_FILTER: [{ type: Output }]
    };
    return FilterComponent;
}());
export { FilterComponent };
if (false) {
    /** @type {?} */
    FilterComponent.prototype.configOptions;
    /** @type {?} */
    FilterComponent.prototype.EMIT_FILTER;
    /** @type {?} */
    FilterComponent.prototype.filterConfigOption;
    /** @type {?} */
    FilterComponent.prototype.searchKey;
    /** @type {?} */
    FilterComponent.prototype.index;
    /** @type {?} */
    FilterComponent.prototype.options;
    /** @type {?} */
    FilterComponent.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbImZpbHRlci9maWx0ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzdFLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLHFCQUFxQixDQUFDO0FBRXJEO0lBcUJJLHlCQUFvQixTQUEyQjtRQUEzQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQWRyQyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFFaEQsdUJBQWtCLEdBQVE7WUFDdEIsS0FBSyxFQUFFLEVBQUU7WUFDVCxNQUFNLEVBQUUsRUFBRTtZQUNWLEtBQUssRUFBRSxFQUFFO1lBQ1QsT0FBTyxFQUFFLEVBQUU7WUFDWCxlQUFlLEVBQUUsRUFBRTtTQUN0QixDQUFDO1FBSUYsWUFBTyxHQUFlLEVBQUUsQ0FBQztRQUdyQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3BCLENBQUM7Ozs7SUFFRCxrQ0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUM3QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2pFO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxzQ0FBWTs7OztJQUFaLFVBQWEsTUFBZTtRQUN4QixJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNO1lBQ0gsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUM7U0FDckM7SUFDTCxDQUFDOzs7OztJQUVELDJDQUFpQjs7OztJQUFqQixVQUFrQixNQUFNO1FBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7O2dCQUM1QixTQUFTLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDekIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM3RCxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN0RDtZQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDO1NBQzVCO2FBQU07WUFDSCxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztTQUNyQjtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELG9EQUEwQjs7O0lBQTFCO1FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUU7O2dCQUM3RixTQUFTLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDekIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM3RCxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN0RDtZQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDekI7YUFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFOztnQkFDakUsU0FBUyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUMxQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQjthQUFNO1lBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7O0lBRUQsb0NBQVU7Ozs7SUFBVixVQUFXLE1BQU07O1lBQ1AsVUFBVSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUMzQyxJQUFJLE1BQU0sRUFBRTtZQUNSLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN0QjtRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsY0FBYyxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQUM7SUFDcEcsQ0FBQzs7Ozs7SUFFRCxnREFBc0I7Ozs7SUFBdEIsVUFBdUIsTUFBTTs7WUFDckIsWUFBWSxHQUFHLEVBQUU7UUFDckIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxJQUFJLE1BQU0sS0FBSyxLQUFLLEVBQUU7WUFDM0QsWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcseUJBQXlCLENBQUMsQ0FBQyxDQUFDO2dCQUN6SCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUMsQ0FBQztTQUNwRjthQUFNO1lBQ0gsWUFBWSxHQUFHLE1BQU0sQ0FBQztTQUN6QjtRQUNELE9BQU8sWUFBWSxDQUFDO0lBQ3hCLENBQUM7O2dCQTFGSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLHU3QkFBc0M7O2lCQUV6Qzs7OztnQkFOTyxnQkFBZ0I7OztnQ0FRbkIsS0FBSzs4QkFDTCxNQUFNOztJQXFGWCxzQkFBQztDQUFBLEFBNUZELElBNEZDO1NBdkZZLGVBQWU7OztJQUN4Qix3Q0FBK0I7O0lBQy9CLHNDQUFnRDs7SUFFaEQsNkNBTUU7O0lBQ0Ysb0NBQWtCOztJQUNsQixnQ0FBYzs7SUFFZCxrQ0FBeUI7O0lBRWIsb0NBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZX0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYXBwLWZpbHRlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2ZpbHRlci5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vZmlsdGVyLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBGaWx0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIEBJbnB1dCgpIGNvbmZpZ09wdGlvbnM6IE9iamVjdDtcbiAgICBAT3V0cHV0KCkgRU1JVF9GSUxURVIgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICAgIGZpbHRlckNvbmZpZ09wdGlvbjogYW55ID0ge1xuICAgICAgICB0aXRsZTogJycsXG4gICAgICAgIGNvbHVtbjogJycsXG4gICAgICAgIGNsYXNzOiAnJyxcbiAgICAgICAgb3B0aW9uczogW10sXG4gICAgICAgIHNlbGVjdGVkT3B0aW9uczogW11cbiAgICB9O1xuICAgIHNlYXJjaEtleTogc3RyaW5nO1xuICAgIGluZGV4OiBudW1iZXI7XG5cbiAgICBvcHRpb25zOiBBcnJheTxhbnk+ID0gW107XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSkge1xuICAgICAgICB0aGlzLnNlYXJjaEtleSA9ICcnO1xuICAgICAgICB0aGlzLmluZGV4ID0gLTE7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuZmlsdGVyQ29uZmlnT3B0aW9uID0gdGhpcy5jb25maWdPcHRpb25zO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZmlsdGVyQ29uZmlnT3B0aW9uLnNlbGVjdGVkT3B0aW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdGhpcy5vcHRpb25zLnB1c2godGhpcy5maWx0ZXJDb25maWdPcHRpb24uc2VsZWN0ZWRPcHRpb25zW2ldKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNlbGVjdE9wdGlvbihwYXJhbXM6IGJvb2xlYW4pOiB2b2lkIHtcbiAgICAgICAgaWYgKHBhcmFtcykge1xuICAgICAgICAgICAgdGhpcy5zZWxlY3REZXNlbGVjdEFsbChwYXJhbXMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zZWxlY3REZXNlbGVjdE90aGVyT3B0aW9ucygpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2VsZWN0RGVzZWxlY3RBbGwocGFyYW1zKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuaW5kZXhPZignQWxsJykgPiAtMSkge1xuICAgICAgICAgICAgY29uc3QgdG1wT3B0aW9uID0gWydBbGwnXTtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5maWx0ZXJDb25maWdPcHRpb24ub3B0aW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIHRtcE9wdGlvbi5wdXNoKHRoaXMuZmlsdGVyQ29uZmlnT3B0aW9uLm9wdGlvbnNbaV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gdG1wT3B0aW9uO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gW107XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5lbWl0RmlsdGVyKHBhcmFtcyk7XG4gICAgfVxuXG4gICAgc2VsZWN0RGVzZWxlY3RPdGhlck9wdGlvbnMoKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMubGVuZ3RoID09PSB0aGlzLmZpbHRlckNvbmZpZ09wdGlvbi5vcHRpb25zLmxlbmd0aCAmJiB0aGlzLm9wdGlvbnMuaW5kZXhPZignQWxsJykgPCAwKSB7XG4gICAgICAgICAgICBjb25zdCB0bXBPcHRpb24gPSBbJ0FsbCddO1xuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmZpbHRlckNvbmZpZ09wdGlvbi5vcHRpb25zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdG1wT3B0aW9uLnB1c2godGhpcy5maWx0ZXJDb25maWdPcHRpb24ub3B0aW9uc1tpXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSB0bXBPcHRpb247XG4gICAgICAgICAgICB0aGlzLmVtaXRGaWx0ZXIodHJ1ZSk7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5vcHRpb25zLmxlbmd0aCA9PT0gdGhpcy5maWx0ZXJDb25maWdPcHRpb24ub3B0aW9ucy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGNvbnN0IHRtcE9wdGlvbiA9IEFycmF5LmZyb20odGhpcy5vcHRpb25zKTtcbiAgICAgICAgICAgIHRtcE9wdGlvbi5zaGlmdCgpO1xuICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gdG1wT3B0aW9uO1xuICAgICAgICAgICAgdGhpcy5lbWl0RmlsdGVyKGZhbHNlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZW1pdEZpbHRlcihmYWxzZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBlbWl0RmlsdGVyKHBhcmFtcyk6IHZvaWQge1xuICAgICAgICBjb25zdCBleGNsdWRlQWxsID0gQXJyYXkuZnJvbSh0aGlzLm9wdGlvbnMpO1xuICAgICAgICBpZiAocGFyYW1zKSB7XG4gICAgICAgICAgICBleGNsdWRlQWxsLnNoaWZ0KCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLkVNSVRfRklMVEVSLmVtaXQoe2NvbHVtbk5hbWU6IHRoaXMuZmlsdGVyQ29uZmlnT3B0aW9uLmNvbHVtbiwgc2VsZWN0ZWRPcHRpb246IGV4Y2x1ZGVBbGx9KTtcbiAgICB9XG5cbiAgICB0cmFuc2xhdGVGaWx0ZXJPcHRpb25zKG9wdGlvbik6IHN0cmluZyB7XG4gICAgICAgIGxldCB0cmFuc2xhdGVTdHIgPSAnJztcbiAgICAgICAgaWYgKHRoaXMuZmlsdGVyQ29uZmlnT3B0aW9uLnRyYW5zbGF0ZVBhdGggJiYgb3B0aW9uICE9PSAnQWxsJykge1xuICAgICAgICAgICAgdHJhbnNsYXRlU3RyID0gdGhpcy5maWx0ZXJDb25maWdPcHRpb24uY29sdW1uID09PSAnZXZlbnRUeXBlcycgPyB0aGlzLnRyYW5zbGF0ZS5pbnN0YW50KG9wdGlvbiArICcuQWxlcnRfVHlwZV9EZXNjcmlwdGlvbicpIDpcbiAgICAgICAgICAgICAgICB0aGlzLnRyYW5zbGF0ZS5pbnN0YW50KHRoaXMuZmlsdGVyQ29uZmlnT3B0aW9uLnRyYW5zbGF0ZVBhdGggKyAnLicgKyBvcHRpb24pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdHJhbnNsYXRlU3RyID0gb3B0aW9uO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0cmFuc2xhdGVTdHI7XG4gICAgfVxuXG59XG4iXX0=