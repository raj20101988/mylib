/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var LoaderComponent = /** @class */ (function () {
    function LoaderComponent() {
    }
    /**
     * @param {?} count
     * @return {?}
     */
    LoaderComponent.prototype.generateFake = /**
     * @param {?} count
     * @return {?}
     */
    function (count) {
        /** @type {?} */
        var indexes = [];
        for (var i = 0; i < count; i++) {
            indexes.push(i);
        }
        return indexes;
    };
    LoaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-loader',
                    template: "<div class=\"skeleton\">\n    <div *ngFor=\"let fake of generateFake(4)\" class=\"card-layout\">\n        <h3></h3>\n        <h4></h4>\n        <p></p>\n    </div>\n</div>\n",
                    styles: [".skeleton{padding:0 10%}.skeleton h3,.skeleton h4,.skeleton p{-webkit-animation:1.7s linear infinite loading;animation:1.7s linear infinite loading;background:no-repeat #f6f7f8;background-image:linear-gradient(to left,#f6f7f8 0,#edeef1 20%,#f6f7f8 40%,#f6f7f8 100%);width:100%;margin-bottom:5px}.skeleton h3{height:22px;width:40%}.skeleton h4{height:18px;width:65%}.skeleton p{height:18px;margin-bottom:50px}@-webkit-keyframes loading{0%{background-position:-100px}100%{background-position:200px}}@keyframes loading{0%{background-position:-100px}100%{background-position:200px}}"]
                }] }
    ];
    /** @nocollapse */
    LoaderComponent.ctorParameters = function () { return []; };
    return LoaderComponent;
}());
export { LoaderComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbImxvYWRlci9sb2FkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRXhDO0lBTUk7SUFDQSxDQUFDOzs7OztJQUVELHNDQUFZOzs7O0lBQVosVUFBYSxLQUFhOztZQUNoQixPQUFPLEdBQUcsRUFBRTtRQUNsQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbkI7UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOztnQkFmSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLFlBQVk7b0JBQ3RCLHlMQUFzQzs7aUJBRXpDOzs7O0lBWUQsc0JBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQVhZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYXBwLWxvYWRlcicsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvYWRlci5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbG9hZGVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTG9hZGVyQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICB9XG5cbiAgICBnZW5lcmF0ZUZha2UoY291bnQ6IG51bWJlcik6IEFycmF5PG51bWJlcj4ge1xuICAgICAgICBjb25zdCBpbmRleGVzID0gW107XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY291bnQ7IGkrKykge1xuICAgICAgICAgICAgaW5kZXhlcy5wdXNoKGkpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBpbmRleGVzO1xuICAgIH1cbn1cbiJdfQ==