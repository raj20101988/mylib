/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function Alert() { }
if (false) {
    /** @type {?} */
    Alert.prototype.alertId;
    /** @type {?} */
    Alert.prototype.eventTypeId;
    /** @type {?} */
    Alert.prototype.eventType;
    /** @type {?} */
    Alert.prototype.displayMeta;
    /** @type {?} */
    Alert.prototype.eventSourceApplication;
    /** @type {?} */
    Alert.prototype.eventSourceHost;
    /** @type {?} */
    Alert.prototype.alertSeverity;
    /** @type {?} */
    Alert.prototype.alertStatus;
    /** @type {?} */
    Alert.prototype.createdDtm;
    /** @type {?} */
    Alert.prototype.customerKnowledgeBase;
    /** @type {?} */
    Alert.prototype.eventData;
    /** @type {?} */
    Alert.prototype.topologyId;
    /** @type {?} */
    Alert.prototype.topologyName;
    /** @type {?} */
    Alert.prototype.topologyPath;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsiaW50ZXJmYWNlL2FsZXJ0LmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsMkJBZUM7OztJQWRDLHdCQUFnQjs7SUFDaEIsNEJBQW9COztJQUNwQiwwQkFBa0I7O0lBQ2xCLDRCQUFvQjs7SUFDcEIsdUNBQStCOztJQUMvQixnQ0FBd0I7O0lBQ3hCLDhCQUFzQjs7SUFDdEIsNEJBQW9COztJQUNwQiwyQkFBaUI7O0lBQ2pCLHNDQUE4Qjs7SUFDOUIsMEJBQXlCOztJQUN6QiwyQkFBbUI7O0lBQ25CLDZCQUFxQjs7SUFDckIsNkJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBBbGVydCB7XG4gIGFsZXJ0SWQ6IG51bWJlcjtcbiAgZXZlbnRUeXBlSWQ6IG51bWJlcjtcbiAgZXZlbnRUeXBlOiBzdHJpbmc7XG4gIGRpc3BsYXlNZXRhOiBzdHJpbmc7XG4gIGV2ZW50U291cmNlQXBwbGljYXRpb246IHN0cmluZztcbiAgZXZlbnRTb3VyY2VIb3N0OiBzdHJpbmc7XG4gIGFsZXJ0U2V2ZXJpdHk6IHN0cmluZztcbiAgYWxlcnRTdGF0dXM6IHN0cmluZztcbiAgY3JlYXRlZER0bTogRGF0ZTtcbiAgY3VzdG9tZXJLbm93bGVkZ2VCYXNlOiBzdHJpbmc7XG4gIGV2ZW50RGF0YTogQXJyYXk8b2JqZWN0PjtcbiAgdG9wb2xvZ3lJZDogbnVtYmVyO1xuICB0b3BvbG9neU5hbWU6IHN0cmluZztcbiAgdG9wb2xvZ3lQYXRoOiBzdHJpbmc7XG59XG4iXX0=