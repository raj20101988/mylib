/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var SortingService = /** @class */ (function () {
    function SortingService() {
    }
    /**
     * @param {?} sort
     * @return {?}
     */
    SortingService.prototype.getSortObject = /**
     * @param {?} sort
     * @return {?}
     */
    function (sort) {
        this.sortObj = { sortOrder: sort.direction.toUpperCase(), sortField: sort.active };
        return this.sortObj;
    };
    SortingService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SortingService.ctorParameters = function () { return []; };
    /** @nocollapse */ SortingService.ngInjectableDef = i0.defineInjectable({ factory: function SortingService_Factory() { return new SortingService(); }, token: SortingService, providedIn: "root" });
    return SortingService;
}());
export { SortingService };
if (false) {
    /** @type {?} */
    SortingService.prototype.sortObj;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ydGluZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsic29ydGluZy9zb3J0aW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBRTNDO0lBS0U7SUFBZ0IsQ0FBQzs7Ozs7SUFDakIsc0NBQWE7Ozs7SUFBYixVQUFjLElBQUk7UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDbEYsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3RCLENBQUM7O2dCQVRGLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7O3lCQUpEO0NBWUMsQUFWRCxJQVVDO1NBUFksY0FBYzs7O0lBQ3ZCLGlDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgU29ydGluZ1NlcnZpY2Uge1xuICAgIHNvcnRPYmo6IG9iamVjdDtcbiAgY29uc3RydWN0b3IoKSB7IH1cbiAgZ2V0U29ydE9iamVjdChzb3J0KTogb2JqZWN0IHtcbiAgICB0aGlzLnNvcnRPYmogPSB7c29ydE9yZGVyOiBzb3J0LmRpcmVjdGlvbi50b1VwcGVyQ2FzZSgpLCBzb3J0RmllbGQ6IHNvcnQuYWN0aXZlIH07XG4gICAgcmV0dXJuIHRoaXMuc29ydE9iajtcbiAgfVxufVxuIl19