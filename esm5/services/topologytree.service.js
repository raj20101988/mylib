/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/**
 * @record
 */
export function ITopologyNodes() { }
if (false) {
    /** @type {?} */
    ITopologyNodes.prototype.meta;
    /** @type {?} */
    ITopologyNodes.prototype.nodeId;
    /** @type {?} */
    ITopologyNodes.prototype.name;
    /** @type {?} */
    ITopologyNodes.prototype.shortName;
    /** @type {?} */
    ITopologyNodes.prototype.type;
    /** @type {?} */
    ITopologyNodes.prototype.description;
    /** @type {?} */
    ITopologyNodes.prototype.deleted;
    /** @type {?} */
    ITopologyNodes.prototype.retired;
    /** @type {?} */
    ITopologyNodes.prototype.parentNodeId;
    /** @type {?} */
    ITopologyNodes.prototype.parentNodeHref;
    /** @type {?} */
    ITopologyNodes.prototype.path;
    /** @type {?} */
    ITopologyNodes.prototype.childNodesHrefs;
    /** @type {?} */
    ITopologyNodes.prototype.createdDtm;
    /** @type {?} */
    ITopologyNodes.prototype.updatedDtm;
    /** @type {?} */
    ITopologyNodes.prototype.status;
}
/**
 * AVG:(Access or Virtual Group) nodes, this interface has common properties for access and virtual group.
 * @record
 */
export function ITopologyAVGNodes() { }
if (false) {
    /** @type {?} */
    ITopologyAVGNodes.prototype.createdDtm;
    /** @type {?} */
    ITopologyAVGNodes.prototype.groupName;
    /** @type {?} */
    ITopologyAVGNodes.prototype.groupType;
    /** @type {?} */
    ITopologyAVGNodes.prototype.meta;
    /** @type {?} */
    ITopologyAVGNodes.prototype.topologyGroupId;
    /** @type {?} */
    ITopologyAVGNodes.prototype.topologyId;
    /** @type {?} */
    ITopologyAVGNodes.prototype.topologyNodeIds;
    /** @type {?} */
    ITopologyAVGNodes.prototype.updatedDtm;
    /** @type {?} */
    ITopologyAVGNodes.prototype.userId;
}
/**
 * if you need to provide it at global level do not specify the module here just write the 'root', this is restricted to module if you
 * want to use the service need to import TopologyTreeModule.
 */
var TopologytreeService = /** @class */ (function () {
    function TopologytreeService(_http) {
        this._http = _http;
    }
    // getTopologyNode: getTopologyNode,
    // getTopologyNodeByNodeName: getTopologyNodeByNodeName,
    // getTopologyNodeByNodeId: getTopologyNodeByNodeId,
    // getTopologyNodes: getTopologyNodes,
    // getTopologyNodeByHostName: getTopologyNodeByHostName,
    // getTopologyNodesFromCache: getTopologyNodesFromCache,
    // getTopologyTypes: getTopologyTypes,
    // updateTopologyNode: updateTopologyNode,
    // getUnAssignedDevices: getUnAssignedDevices,
    // addTopologyNode: addTopologyNode,
    // deleteNode: deleteNode,
    // getTopologyNodeUnassigned: getTopologyNodeUnassigned,
    // getNodeByTopologyType: getNodeByTopologyType,
    // editTopologyNode: editTopologyNode,
    // createAccessGroup: createAccessGroup,
    // getAccessGroup: getAccessGroup,
    // getBonusGroup: getBonusGroup,
    // getAccessGroupById: getAccessGroupById,
    // updateAccessGroup: updateAccessGroup,
    // deleteAccessGroup: deleteAccessGroup,
    // getTopologyDetails: getTopologyDetails,
    // updateTableStatus:updateTableStatus,
    // getDescendantNodesOfTGroup: getDescendantNodesOfTGroup
    // getTopologyNode: getTopologyNode,
    // getTopologyNodeByNodeName: getTopologyNodeByNodeName,
    // getTopologyNodeByNodeId: getTopologyNodeByNodeId,
    // getTopologyNodes: getTopologyNodes,
    // getTopologyNodeByHostName: getTopologyNodeByHostName,
    // getTopologyNodesFromCache: getTopologyNodesFromCache,
    // getTopologyTypes: getTopologyTypes,
    // updateTopologyNode: updateTopologyNode,
    // getUnAssignedDevices: getUnAssignedDevices,
    // addTopologyNode: addTopologyNode,
    // deleteNode: deleteNode,
    // getTopologyNodeUnassigned: getTopologyNodeUnassigned,
    // getNodeByTopologyType: getNodeByTopologyType,
    // editTopologyNode: editTopologyNode,
    // createAccessGroup: createAccessGroup,
    // getAccessGroup: getAccessGroup,
    // getBonusGroup: getBonusGroup,
    // getAccessGroupById: getAccessGroupById,
    // updateAccessGroup: updateAccessGroup,
    // deleteAccessGroup: deleteAccessGroup,
    // getTopologyDetails: getTopologyDetails,
    // updateTableStatus:updateTableStatus,
    // getDescendantNodesOfTGroup: getDescendantNodesOfTGroup
    /**
     * @param {?} url
     * @return {?}
     */
    TopologytreeService.prototype.getTopologyNodes = 
    // getTopologyNode: getTopologyNode,
    // getTopologyNodeByNodeName: getTopologyNodeByNodeName,
    // getTopologyNodeByNodeId: getTopologyNodeByNodeId,
    // getTopologyNodes: getTopologyNodes,
    // getTopologyNodeByHostName: getTopologyNodeByHostName,
    // getTopologyNodesFromCache: getTopologyNodesFromCache,
    // getTopologyTypes: getTopologyTypes,
    // updateTopologyNode: updateTopologyNode,
    // getUnAssignedDevices: getUnAssignedDevices,
    // addTopologyNode: addTopologyNode,
    // deleteNode: deleteNode,
    // getTopologyNodeUnassigned: getTopologyNodeUnassigned,
    // getNodeByTopologyType: getNodeByTopologyType,
    // editTopologyNode: editTopologyNode,
    // createAccessGroup: createAccessGroup,
    // getAccessGroup: getAccessGroup,
    // getBonusGroup: getBonusGroup,
    // getAccessGroupById: getAccessGroupById,
    // updateAccessGroup: updateAccessGroup,
    // deleteAccessGroup: deleteAccessGroup,
    // getTopologyDetails: getTopologyDetails,
    // updateTableStatus:updateTableStatus,
    // getDescendantNodesOfTGroup: getDescendantNodesOfTGroup
    /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        return (/** @type {?} */ (this._http.get(url)));
    };
    /**
     * @param {?} url
     * @return {?}
     */
    TopologytreeService.prototype.getTopologyTypes = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        return (/** @type {?} */ (this._http.get(url)));
    };
    /**
     * @param {?} url
     * @param {?=} params
     * @return {?}
     */
    TopologytreeService.prototype.getTopologyNodeByNodeId = /**
     * @param {?} url
     * @param {?=} params
     * @return {?}
     */
    function (url, params) {
        if (params === void 0) { params = null; }
        return (/** @type {?} */ (this._http.get(url, { params: params })));
    };
    /**
     * @param {?} url
     * @param {?=} params
     * @return {?}
     */
    TopologytreeService.prototype.getAccessOrVirtualGroupNodes = /**
     * @param {?} url
     * @param {?=} params
     * @return {?}
     */
    function (url, params) {
        if (params === void 0) { params = null; }
        return (/** @type {?} */ (this._http.get(url, { params: params })));
    };
    /**
     * @param {?} url
     * @return {?}
     */
    TopologytreeService.prototype.getTopologyNodeUnassigned = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        return (/** @type {?} */ (this._http.get(url)));
    };
    TopologytreeService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TopologytreeService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ TopologytreeService.ngInjectableDef = i0.defineInjectable({ factory: function TopologytreeService_Factory() { return new TopologytreeService(i0.inject(i1.HttpClient)); }, token: TopologytreeService, providedIn: "root" });
    return TopologytreeService;
}());
export { TopologytreeService };
if (false) {
    /** @type {?} */
    TopologytreeService.prototype._http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9wb2xvZ3l0cmVlLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJzZXJ2aWNlcy90b3BvbG9neXRyZWUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUMsVUFBVSxFQUEyQixNQUFNLHNCQUFzQixDQUFDOzs7Ozs7QUFHMUUsb0NBbUJDOzs7SUFsQkcsOEJBR0U7O0lBQ0YsZ0NBQWU7O0lBQ2YsOEJBQWE7O0lBQ2IsbUNBQWtCOztJQUNsQiw4QkFBYTs7SUFDYixxQ0FBb0I7O0lBQ3BCLGlDQUFpQjs7SUFDakIsaUNBQWlCOztJQUNqQixzQ0FBcUI7O0lBQ3JCLHdDQUF1Qjs7SUFDdkIsOEJBQWE7O0lBQ2IseUNBQTRCOztJQUM1QixvQ0FBbUI7O0lBQ25CLG9DQUFtQjs7SUFDbkIsZ0NBQWU7Ozs7OztBQU9uQix1Q0FhQzs7O0lBWkcsdUNBQW1COztJQUNuQixzQ0FBa0I7O0lBQ2xCLHNDQUFrQjs7SUFDbEIsaUNBR0U7O0lBQ0YsNENBQXdCOztJQUN4Qix1Q0FBbUI7O0lBQ25CLDRDQUE0Qjs7SUFDNUIsdUNBQW1COztJQUNuQixtQ0FBZTs7Ozs7O0FBT25CO0lBSUUsNkJBQW9CLEtBQWlCO1FBQWpCLFVBQUssR0FBTCxLQUFLLENBQVk7SUFBSSxDQUFDO0lBRXhDLG9DQUFvQztJQUNwQyx3REFBd0Q7SUFDeEQsb0RBQW9EO0lBQ3BELHNDQUFzQztJQUN0Qyx3REFBd0Q7SUFDeEQsd0RBQXdEO0lBQ3hELHNDQUFzQztJQUN0QywwQ0FBMEM7SUFDMUMsOENBQThDO0lBQzlDLG9DQUFvQztJQUNwQywwQkFBMEI7SUFDMUIsd0RBQXdEO0lBQ3hELGdEQUFnRDtJQUNoRCxzQ0FBc0M7SUFDdEMsd0NBQXdDO0lBQ3hDLGtDQUFrQztJQUNsQyxnQ0FBZ0M7SUFDaEMsMENBQTBDO0lBQzFDLHdDQUF3QztJQUN4Qyx3Q0FBd0M7SUFDeEMsMENBQTBDO0lBQzFDLHVDQUF1QztJQUN2Qyx5REFBeUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFDekQsOENBQWdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBQWhCLFVBQWlCLEdBQUc7UUFDaEIsT0FBTyxtQkFBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBZ0MsQ0FBQztJQUMvRCxDQUFDOzs7OztJQUNELDhDQUFnQjs7OztJQUFoQixVQUFpQixHQUFHO1FBQ2hCLE9BQU8sbUJBQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQWdDLENBQUM7SUFDL0QsQ0FBQzs7Ozs7O0lBRUQscURBQXVCOzs7OztJQUF2QixVQUF3QixHQUFHLEVBQUUsTUFBeUI7UUFBekIsdUJBQUEsRUFBQSxhQUF5QjtRQUNsRCxPQUFPLG1CQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLE1BQU0sUUFBQSxFQUFFLENBQUMsRUFBZ0MsQ0FBQztJQUMzRSxDQUFDOzs7Ozs7SUFDRCwwREFBNEI7Ozs7O0lBQTVCLFVBQTZCLEdBQUcsRUFBRSxNQUF5QjtRQUF6Qix1QkFBQSxFQUFBLGFBQXlCO1FBQ3ZELE9BQU8sbUJBQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsTUFBTSxRQUFBLEVBQUUsQ0FBQyxFQUFtQyxDQUFDO0lBQzlFLENBQUM7Ozs7O0lBQ0QsdURBQXlCOzs7O0lBQXpCLFVBQTBCLEdBQUc7UUFDekIsT0FBTyxtQkFBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBZ0MsQ0FBQztJQUMvRCxDQUFDOztnQkE1Q0osVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFqRE8sVUFBVTs7OzhCQURsQjtDQTZGQyxBQTdDRCxJQTZDQztTQTFDWSxtQkFBbUI7OztJQUNsQixvQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBDbGllbnQsIEh0dHBQYXJhbXMsIEh0dHBSZXNwb25zZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tICdyeGpzJztcblxuZXhwb3J0IGludGVyZmFjZSBJVG9wb2xvZ3lOb2RlcyB7XG4gICAgbWV0YToge1xuICAgICAgICBocmVmOiBzdHJpbmcsXG4gICAgICAgIG1lZGlhVHlwZTogc3RyaW5nXG4gICAgfTtcbiAgICBub2RlSWQ6IHN0cmluZztcbiAgICBuYW1lOiBzdHJpbmc7XG4gICAgc2hvcnROYW1lOiBzdHJpbmc7XG4gICAgdHlwZTogbnVtYmVyO1xuICAgIGRlc2NyaXB0aW9uOiBzdHJpbmc7XG4gICAgZGVsZXRlZDogYm9vbGVhbjtcbiAgICByZXRpcmVkOiBib29sZWFuO1xuICAgIHBhcmVudE5vZGVJZDogbnVtYmVyO1xuICAgIHBhcmVudE5vZGVIcmVmOiBzdHJpbmc7XG4gICAgcGF0aDogc3RyaW5nO1xuICAgIGNoaWxkTm9kZXNIcmVmczogQXJyYXk8YW55PjtcbiAgICBjcmVhdGVkRHRtOiBzdHJpbmc7XG4gICAgdXBkYXRlZER0bTogc3RyaW5nO1xuICAgIHN0YXR1czogc3RyaW5nO1xufVxuXG5cbi8qKlxuICogQVZHOihBY2Nlc3Mgb3IgVmlydHVhbCBHcm91cCkgbm9kZXMsIHRoaXMgaW50ZXJmYWNlIGhhcyBjb21tb24gcHJvcGVydGllcyBmb3IgYWNjZXNzIGFuZCB2aXJ0dWFsIGdyb3VwLlxuICovXG5leHBvcnQgaW50ZXJmYWNlIElUb3BvbG9neUFWR05vZGVzIHtcbiAgICBjcmVhdGVkRHRtOiBzdHJpbmc7XG4gICAgZ3JvdXBOYW1lOiBzdHJpbmc7XG4gICAgZ3JvdXBUeXBlOiBzdHJpbmc7XG4gICAgbWV0YToge1xuICAgICAgICBocmVmOiBzdHJpbmcsXG4gICAgICAgIG1lZGlhVHlwZTogc3RyaW5nXG4gICAgfTtcbiAgICB0b3BvbG9neUdyb3VwSWQ6IG51bWJlcjtcbiAgICB0b3BvbG9neUlkOiBudW1iZXI7XG4gICAgdG9wb2xvZ3lOb2RlSWRzOiBBcnJheTxhbnk+O1xuICAgIHVwZGF0ZWREdG06IHN0cmluZztcbiAgICB1c2VySWQ6IG51bWJlcjtcbn1cblxuLyoqXG4gKiBpZiB5b3UgbmVlZCB0byBwcm92aWRlIGl0IGF0IGdsb2JhbCBsZXZlbCBkbyBub3Qgc3BlY2lmeSB0aGUgbW9kdWxlIGhlcmUganVzdCB3cml0ZSB0aGUgJ3Jvb3QnLCB0aGlzIGlzIHJlc3RyaWN0ZWQgdG8gbW9kdWxlIGlmIHlvdVxuICogd2FudCB0byB1c2UgdGhlIHNlcnZpY2UgbmVlZCB0byBpbXBvcnQgVG9wb2xvZ3lUcmVlTW9kdWxlLlxuICovXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBUb3BvbG9neXRyZWVTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBfaHR0cDogSHR0cENsaWVudCkgeyB9XG5cbiAgICAvLyBnZXRUb3BvbG9neU5vZGU6IGdldFRvcG9sb2d5Tm9kZSxcbiAgICAvLyBnZXRUb3BvbG9neU5vZGVCeU5vZGVOYW1lOiBnZXRUb3BvbG9neU5vZGVCeU5vZGVOYW1lLFxuICAgIC8vIGdldFRvcG9sb2d5Tm9kZUJ5Tm9kZUlkOiBnZXRUb3BvbG9neU5vZGVCeU5vZGVJZCxcbiAgICAvLyBnZXRUb3BvbG9neU5vZGVzOiBnZXRUb3BvbG9neU5vZGVzLFxuICAgIC8vIGdldFRvcG9sb2d5Tm9kZUJ5SG9zdE5hbWU6IGdldFRvcG9sb2d5Tm9kZUJ5SG9zdE5hbWUsXG4gICAgLy8gZ2V0VG9wb2xvZ3lOb2Rlc0Zyb21DYWNoZTogZ2V0VG9wb2xvZ3lOb2Rlc0Zyb21DYWNoZSxcbiAgICAvLyBnZXRUb3BvbG9neVR5cGVzOiBnZXRUb3BvbG9neVR5cGVzLFxuICAgIC8vIHVwZGF0ZVRvcG9sb2d5Tm9kZTogdXBkYXRlVG9wb2xvZ3lOb2RlLFxuICAgIC8vIGdldFVuQXNzaWduZWREZXZpY2VzOiBnZXRVbkFzc2lnbmVkRGV2aWNlcyxcbiAgICAvLyBhZGRUb3BvbG9neU5vZGU6IGFkZFRvcG9sb2d5Tm9kZSxcbiAgICAvLyBkZWxldGVOb2RlOiBkZWxldGVOb2RlLFxuICAgIC8vIGdldFRvcG9sb2d5Tm9kZVVuYXNzaWduZWQ6IGdldFRvcG9sb2d5Tm9kZVVuYXNzaWduZWQsXG4gICAgLy8gZ2V0Tm9kZUJ5VG9wb2xvZ3lUeXBlOiBnZXROb2RlQnlUb3BvbG9neVR5cGUsXG4gICAgLy8gZWRpdFRvcG9sb2d5Tm9kZTogZWRpdFRvcG9sb2d5Tm9kZSxcbiAgICAvLyBjcmVhdGVBY2Nlc3NHcm91cDogY3JlYXRlQWNjZXNzR3JvdXAsXG4gICAgLy8gZ2V0QWNjZXNzR3JvdXA6IGdldEFjY2Vzc0dyb3VwLFxuICAgIC8vIGdldEJvbnVzR3JvdXA6IGdldEJvbnVzR3JvdXAsXG4gICAgLy8gZ2V0QWNjZXNzR3JvdXBCeUlkOiBnZXRBY2Nlc3NHcm91cEJ5SWQsXG4gICAgLy8gdXBkYXRlQWNjZXNzR3JvdXA6IHVwZGF0ZUFjY2Vzc0dyb3VwLFxuICAgIC8vIGRlbGV0ZUFjY2Vzc0dyb3VwOiBkZWxldGVBY2Nlc3NHcm91cCxcbiAgICAvLyBnZXRUb3BvbG9neURldGFpbHM6IGdldFRvcG9sb2d5RGV0YWlscyxcbiAgICAvLyB1cGRhdGVUYWJsZVN0YXR1czp1cGRhdGVUYWJsZVN0YXR1cyxcbiAgICAvLyBnZXREZXNjZW5kYW50Tm9kZXNPZlRHcm91cDogZ2V0RGVzY2VuZGFudE5vZGVzT2ZUR3JvdXBcbiAgICBnZXRUb3BvbG9neU5vZGVzKHVybCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQodXJsKSBhcyBPYnNlcnZhYmxlPElUb3BvbG9neU5vZGVzW10+O1xuICAgIH1cbiAgICBnZXRUb3BvbG9neVR5cGVzKHVybCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQodXJsKSBhcyBPYnNlcnZhYmxlPElUb3BvbG9neU5vZGVzW10+O1xuICAgIH1cblxuICAgIGdldFRvcG9sb2d5Tm9kZUJ5Tm9kZUlkKHVybCwgcGFyYW1zOiBIdHRwUGFyYW1zID0gbnVsbCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQodXJsLCB7IHBhcmFtcyB9KSBhcyBPYnNlcnZhYmxlPElUb3BvbG9neU5vZGVzW10+O1xuICAgIH1cbiAgICBnZXRBY2Nlc3NPclZpcnR1YWxHcm91cE5vZGVzKHVybCwgcGFyYW1zOiBIdHRwUGFyYW1zID0gbnVsbCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQodXJsLCB7IHBhcmFtcyB9KSBhcyBPYnNlcnZhYmxlPElUb3BvbG9neUFWR05vZGVzW10+O1xuICAgIH1cbiAgICBnZXRUb3BvbG9neU5vZGVVbmFzc2lnbmVkKHVybCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQodXJsKSBhcyBPYnNlcnZhYmxlPElUb3BvbG9neU5vZGVzW10+O1xuICAgIH1cbn1cbiJdfQ==