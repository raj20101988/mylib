/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as i0 from "@angular/core";
import * as i1 from "@auth0/angular-jwt/src/jwthelper.service";
/**
 * This service adds jwt token on every API call
 */
var JwtInterceptorService = /** @class */ (function () {
    function JwtInterceptorService(jwtHelper) {
        this.jwtHelper = jwtHelper;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    JwtInterceptorService.prototype.intercept = /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    function (req, next) {
        /** @type {?} */
        var jwtToken = this.jwtHelper.tokenGetter();
        req = req.clone({
            setHeaders: {
                Authorization: "Bearer " + jwtToken
            }
        });
        return next.handle(req);
    };
    JwtInterceptorService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    JwtInterceptorService.ctorParameters = function () { return [
        { type: JwtHelperService }
    ]; };
    /** @nocollapse */ JwtInterceptorService.ngInjectableDef = i0.defineInjectable({ factory: function JwtInterceptorService_Factory() { return new JwtInterceptorService(i0.inject(i1.JwtHelperService)); }, token: JwtInterceptorService, providedIn: "root" });
    return JwtInterceptorService;
}());
export { JwtInterceptorService };
if (false) {
    /** @type {?} */
    JwtInterceptorService.prototype.jwtHelper;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LWludGVyY2VwdG9yLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9BdXRoZW50aWNhdGlvbi9qd3QtaW50ZXJjZXB0b3Iuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUd6QyxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQzs7Ozs7O0FBTXBEO0lBS0ksK0JBQW9CLFNBQTJCO1FBQTNCLGNBQVMsR0FBVCxTQUFTLENBQWtCO0lBQy9DLENBQUM7Ozs7OztJQUVELHlDQUFTOzs7OztJQUFULFVBQVUsR0FBcUIsRUFBRSxJQUFpQjs7WUFDeEMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFO1FBQzdDLEdBQUcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ1osVUFBVSxFQUFFO2dCQUNSLGFBQWEsRUFBRSxZQUFVLFFBQVU7YUFDdEM7U0FDSixDQUFDLENBQUM7UUFFSCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7Z0JBakJKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBUk8sZ0JBQWdCOzs7Z0NBSHhCO0NBNEJDLEFBbkJELElBbUJDO1NBaEJZLHFCQUFxQjs7O0lBRWxCLDBDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBFdmVudCwgSHR0cEhhbmRsZXIsIEh0dHBJbnRlcmNlcHRvciwgSHR0cFJlcXVlc3R9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0p3dEhlbHBlclNlcnZpY2V9IGZyb20gJ0BhdXRoMC9hbmd1bGFyLWp3dCc7XG5cbi8qKlxuICogVGhpcyBzZXJ2aWNlIGFkZHMgand0IHRva2VuIG9uIGV2ZXJ5IEFQSSBjYWxsXG4gKi9cblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBKd3RJbnRlcmNlcHRvclNlcnZpY2UgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBqd3RIZWxwZXI6IEp3dEhlbHBlclNlcnZpY2UpIHtcbiAgICB9XG5cbiAgICBpbnRlcmNlcHQocmVxOiBIdHRwUmVxdWVzdDxhbnk+LCBuZXh0OiBIdHRwSGFuZGxlcik6IE9ic2VydmFibGU8SHR0cEV2ZW50PGFueT4+IHtcbiAgICAgICAgY29uc3Qgand0VG9rZW4gPSB0aGlzLmp3dEhlbHBlci50b2tlbkdldHRlcigpO1xuICAgICAgICByZXEgPSByZXEuY2xvbmUoe1xuICAgICAgICAgICAgc2V0SGVhZGVyczoge1xuICAgICAgICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHtqd3RUb2tlbn1gXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXEpO1xuICAgIH1cblxufVxuIl19