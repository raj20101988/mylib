/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { protocol, webServerDNS } from '../../constants/url.common.constants';
import * as i0 from "@angular/core";
import * as i1 from "@auth0/angular-jwt/src/jwthelper.service";
var TokenHandlingService = /** @class */ (function () {
    function TokenHandlingService(jwtHelper) {
        this.jwtHelper = jwtHelper;
        this.authValues = JSON.parse(sessionStorage.getItem('authValues'));
    }
    /**
     * @return {?}
     */
    TokenHandlingService.prototype.redirectURI = /**
     * @return {?}
     */
    function () {
        location.href = "" + protocol + webServerDNS + '/ldaplogin.html' +
            '?client_id=' + this.authValues.clientId + '&redirect_uri=' + location.href;
    };
    /**
     * @param {?} jwtToken
     * @return {?}
     */
    TokenHandlingService.prototype.isTokenValid = /**
     * @param {?} jwtToken
     * @return {?}
     */
    function (jwtToken) {
        return this.hasPermission(jwtToken) && !this.jwtHelper.isTokenExpired(jwtToken);
    };
    /**
     * @param {?} jwtToken
     * @return {?}
     */
    TokenHandlingService.prototype.hasPermission = /**
     * @param {?} jwtToken
     * @return {?}
     */
    function (jwtToken) {
        /** @type {?} */
        var decodedToken = this.jwtHelper.decodeToken(jwtToken);
        if (decodedToken) {
            if (decodedToken.authorities && ((this.authValues.applicationCode === decodedToken.authorities[0].applicationCode &&
                decodedToken.authorities[0].permissions.includes(this.authValues.accessCode)) || decodedToken.superuser)) {
                return true;
            }
            else {
                return !!(this.authValues.clientId === 'lgn' && decodedToken.applications);
            }
        }
        else {
            return false;
        }
    };
    TokenHandlingService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TokenHandlingService.ctorParameters = function () { return [
        { type: JwtHelperService }
    ]; };
    /** @nocollapse */ TokenHandlingService.ngInjectableDef = i0.defineInjectable({ factory: function TokenHandlingService_Factory() { return new TokenHandlingService(i0.inject(i1.JwtHelperService)); }, token: TokenHandlingService, providedIn: "root" });
    return TokenHandlingService;
}());
export { TokenHandlingService };
if (false) {
    /** @type {?} */
    TokenHandlingService.prototype.authValues;
    /** @type {?} */
    TokenHandlingService.prototype.jwtHelper;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9rZW4taGFuZGxpbmcuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL0F1dGhlbnRpY2F0aW9uL3Rva2VuLWhhbmRsaW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFDLFFBQVEsRUFBRSxZQUFZLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQzs7O0FBRTVFO0lBTUksOEJBQW9CLFNBQTJCO1FBQTNCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQzs7OztJQUVELDBDQUFXOzs7SUFBWDtRQUNJLFFBQVEsQ0FBQyxJQUFJLEdBQUcsS0FBRyxRQUFRLEdBQUcsWUFBYyxHQUFHLGlCQUFpQjtZQUM1RCxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztJQUNwRixDQUFDOzs7OztJQUVELDJDQUFZOzs7O0lBQVosVUFBYSxRQUFRO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3BGLENBQUM7Ozs7O0lBRUQsNENBQWE7Ozs7SUFBYixVQUFjLFFBQVE7O1lBQ1osWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztRQUN6RCxJQUFJLFlBQVksRUFBRTtZQUNkLElBQUksWUFBWSxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEtBQUssWUFBWSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxlQUFlO2dCQUM3RyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLFlBQVksQ0FBQyxTQUFTLENBQUMsRUFBRTtnQkFDMUcsT0FBTyxJQUFJLENBQUM7YUFDZjtpQkFBTTtnQkFDSCxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxLQUFLLEtBQUssSUFBSSxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDOUU7U0FDSjthQUFNO1lBQ0gsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDOztnQkEvQkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFMTyxnQkFBZ0I7OzsrQkFEeEI7Q0FvQ0MsQUFoQ0QsSUFnQ0M7U0E3Qlksb0JBQW9COzs7SUFDN0IsMENBQWdCOztJQUVKLHlDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0p3dEhlbHBlclNlcnZpY2V9IGZyb20gJ0BhdXRoMC9hbmd1bGFyLWp3dCc7XG5pbXBvcnQge3Byb3RvY29sLCB3ZWJTZXJ2ZXJETlN9IGZyb20gJy4uLy4uL2NvbnN0YW50cy91cmwuY29tbW9uLmNvbnN0YW50cyc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVG9rZW5IYW5kbGluZ1NlcnZpY2Uge1xuICAgIGF1dGhWYWx1ZXM6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgand0SGVscGVyOiBKd3RIZWxwZXJTZXJ2aWNlKSB7XG4gICAgICAgIHRoaXMuYXV0aFZhbHVlcyA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgnYXV0aFZhbHVlcycpKTtcbiAgICB9XG5cbiAgICByZWRpcmVjdFVSSSgpOiB2b2lkIHtcbiAgICAgICAgbG9jYXRpb24uaHJlZiA9IGAke3Byb3RvY29sfSR7d2ViU2VydmVyRE5TfWAgKyAnL2xkYXBsb2dpbi5odG1sJyArXG4gICAgICAgICAgICAnP2NsaWVudF9pZD0nICsgdGhpcy5hdXRoVmFsdWVzLmNsaWVudElkICsgJyZyZWRpcmVjdF91cmk9JyArIGxvY2F0aW9uLmhyZWY7XG4gICAgfVxuXG4gICAgaXNUb2tlblZhbGlkKGp3dFRva2VuKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmhhc1Blcm1pc3Npb24oand0VG9rZW4pICYmICF0aGlzLmp3dEhlbHBlci5pc1Rva2VuRXhwaXJlZChqd3RUb2tlbik7XG4gICAgfVxuXG4gICAgaGFzUGVybWlzc2lvbihqd3RUb2tlbik6IGJvb2xlYW4ge1xuICAgICAgICBjb25zdCBkZWNvZGVkVG9rZW4gPSB0aGlzLmp3dEhlbHBlci5kZWNvZGVUb2tlbihqd3RUb2tlbik7XG4gICAgICAgIGlmIChkZWNvZGVkVG9rZW4pIHtcbiAgICAgICAgICAgIGlmIChkZWNvZGVkVG9rZW4uYXV0aG9yaXRpZXMgJiYgKCh0aGlzLmF1dGhWYWx1ZXMuYXBwbGljYXRpb25Db2RlID09PSBkZWNvZGVkVG9rZW4uYXV0aG9yaXRpZXNbMF0uYXBwbGljYXRpb25Db2RlICYmXG4gICAgICAgICAgICAgICAgZGVjb2RlZFRva2VuLmF1dGhvcml0aWVzWzBdLnBlcm1pc3Npb25zLmluY2x1ZGVzKHRoaXMuYXV0aFZhbHVlcy5hY2Nlc3NDb2RlKSkgfHwgZGVjb2RlZFRva2VuLnN1cGVydXNlcikpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICEhKHRoaXMuYXV0aFZhbHVlcy5jbGllbnRJZCA9PT0gJ2xnbicgJiYgZGVjb2RlZFRva2VuLmFwcGxpY2F0aW9ucyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=