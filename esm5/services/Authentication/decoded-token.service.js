/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as i0 from "@angular/core";
import * as i1 from "@auth0/angular-jwt/src/jwthelper.service";
var DecodedTokenService = /** @class */ (function () {
    function DecodedTokenService(jwtHelper) {
        this.jwtHelper = jwtHelper;
    }
    /**
     * @return {?}
     */
    DecodedTokenService.prototype.getDecodedJwtToken = /**
     * @return {?}
     */
    function () {
        return this.decodedJwtToken;
    };
    /**
     * @param {?} token
     * @return {?}
     */
    DecodedTokenService.prototype.setDecodedJwtToken = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        this.decodedJwtToken = this.jwtHelper.decodeToken(token);
    };
    DecodedTokenService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    DecodedTokenService.ctorParameters = function () { return [
        { type: JwtHelperService }
    ]; };
    /** @nocollapse */ DecodedTokenService.ngInjectableDef = i0.defineInjectable({ factory: function DecodedTokenService_Factory() { return new DecodedTokenService(i0.inject(i1.JwtHelperService)); }, token: DecodedTokenService, providedIn: "root" });
    return DecodedTokenService;
}());
export { DecodedTokenService };
if (false) {
    /** @type {?} */
    DecodedTokenService.prototype.decodedJwtToken;
    /** @type {?} */
    DecodedTokenService.prototype.jwtHelper;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVjb2RlZC10b2tlbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsic2VydmljZXMvQXV0aGVudGljYXRpb24vZGVjb2RlZC10b2tlbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLG9CQUFvQixDQUFDOzs7QUFFcEQ7SUFLSSw2QkFBb0IsU0FBMkI7UUFBM0IsY0FBUyxHQUFULFNBQVMsQ0FBa0I7SUFDL0MsQ0FBQzs7OztJQUlELGdEQUFrQjs7O0lBQWxCO1FBQ0ksT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRUQsZ0RBQWtCOzs7O0lBQWxCLFVBQW1CLEtBQUs7UUFDcEIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM3RCxDQUFDOztnQkFoQkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFKTyxnQkFBZ0I7Ozs4QkFEeEI7Q0FvQkMsQUFqQkQsSUFpQkM7U0FkWSxtQkFBbUI7OztJQUs1Qiw4Q0FBd0I7O0lBSFosd0NBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Snd0SGVscGVyU2VydmljZX0gZnJvbSAnQGF1dGgwL2FuZ3VsYXItand0JztcblxuQEluamVjdGFibGUoe1xuICAgIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBEZWNvZGVkVG9rZW5TZXJ2aWNlIHtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgand0SGVscGVyOiBKd3RIZWxwZXJTZXJ2aWNlKSB7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBkZWNvZGVkSnd0VG9rZW47XG5cbiAgICBnZXREZWNvZGVkSnd0VG9rZW4oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRlY29kZWRKd3RUb2tlbjtcbiAgICB9XG5cbiAgICBzZXREZWNvZGVkSnd0VG9rZW4odG9rZW4pIHtcbiAgICAgICAgdGhpcy5kZWNvZGVkSnd0VG9rZW4gPSB0aGlzLmp3dEhlbHBlci5kZWNvZGVUb2tlbih0b2tlbik7XG4gICAgfVxufVxuIl19