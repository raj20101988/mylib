/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { TokenHandlingService } from './token-handling.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { DecodedTokenService } from './decoded-token.service';
import * as i0 from "@angular/core";
import * as i1 from "./token-handling.service";
import * as i2 from "@auth0/angular-jwt/src/jwthelper.service";
import * as i3 from "./decoded-token.service";
var AuthGuard = /** @class */ (function () {
    function AuthGuard(tokenHandlingService, jwtHelper, decodedTokenService) {
        this.tokenHandlingService = tokenHandlingService;
        this.jwtHelper = jwtHelper;
        this.decodedTokenService = decodedTokenService;
        this.jwtToken = location.href.split('access_token=')[1];
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    AuthGuard.prototype.canActivate = /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    function (next, state) {
        this.isTokenValid = this.tokenHandlingService.isTokenValid(this.jwtToken);
        if ((this.jwtToken && this.isTokenValid) ||
            (this.jwtHelper.tokenGetter() && this.tokenHandlingService.isTokenValid(this.jwtHelper.tokenGetter())) ||
            location.href.includes('ppmaster')) {
            if (this.jwtToken) {
                /** @type {?} */
                var authValues = JSON.parse(sessionStorage.getItem('authValues'));
                sessionStorage.setItem(authValues.jwtTokenKey, this.jwtToken);
                this.decodedTokenService.setDecodedJwtToken(this.jwtToken);
            }
            return true;
        }
        else {
            sessionStorage.clear();
            this.tokenHandlingService.redirectURI();
            return false;
        }
    };
    AuthGuard.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthGuard.ctorParameters = function () { return [
        { type: TokenHandlingService },
        { type: JwtHelperService },
        { type: DecodedTokenService }
    ]; };
    /** @nocollapse */ AuthGuard.ngInjectableDef = i0.defineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(i0.inject(i1.TokenHandlingService), i0.inject(i2.JwtHelperService), i0.inject(i3.DecodedTokenService)); }, token: AuthGuard, providedIn: "root" });
    return AuthGuard;
}());
export { AuthGuard };
if (false) {
    /** @type {?} */
    AuthGuard.prototype.jwtToken;
    /** @type {?} */
    AuthGuard.prototype.isTokenValid;
    /** @type {?} */
    AuthGuard.prototype.tokenHandlingService;
    /** @type {?} */
    AuthGuard.prototype.jwtHelper;
    /** @type {?} */
    AuthGuard.prototype.decodedTokenService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL0F1dGhlbnRpY2F0aW9uL2F1dGguZ3VhcmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFHekMsT0FBTyxFQUFDLG9CQUFvQixFQUFDLE1BQU0sMEJBQTBCLENBQUM7QUFDOUQsT0FBTyxFQUFDLGdCQUFnQixFQUFDLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFDLG1CQUFtQixFQUFDLE1BQU0seUJBQXlCLENBQUM7Ozs7O0FBRTVEO0lBT0ksbUJBQW9CLG9CQUEwQyxFQUFVLFNBQTJCLEVBQy9FLG1CQUF3QztRQUR4Qyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDL0Usd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4RCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVELENBQUM7Ozs7OztJQUVELCtCQUFXOzs7OztJQUFYLFVBQ0ksSUFBNEIsRUFDNUIsS0FBMEI7UUFDMUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ3BDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUN0RyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRTtZQUNwQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O29CQUNULFVBQVUsR0FBUSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3hFLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzlELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDOUQ7WUFDRCxPQUFPLElBQUksQ0FBQztTQUNmO2FBQU07WUFDSCxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3hDLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQzs7Z0JBOUJKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTk8sb0JBQW9CO2dCQUNwQixnQkFBZ0I7Z0JBQ2hCLG1CQUFtQjs7O29CQUwzQjtDQXNDQyxBQS9CRCxJQStCQztTQTVCWSxTQUFTOzs7SUFDbEIsNkJBQWlCOztJQUNqQixpQ0FBYTs7SUFFRCx5Q0FBa0Q7O0lBQUUsOEJBQW1DOztJQUN2Rix3Q0FBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBDYW5BY3RpdmF0ZSwgUm91dGVyU3RhdGVTbmFwc2hvdH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge1Rva2VuSGFuZGxpbmdTZXJ2aWNlfSBmcm9tICcuL3Rva2VuLWhhbmRsaW5nLnNlcnZpY2UnO1xuaW1wb3J0IHtKd3RIZWxwZXJTZXJ2aWNlfSBmcm9tICdAYXV0aDAvYW5ndWxhci1qd3QnO1xuaW1wb3J0IHtEZWNvZGVkVG9rZW5TZXJ2aWNlfSBmcm9tICcuL2RlY29kZWQtdG9rZW4uc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQXV0aEd1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xuICAgIGp3dFRva2VuOiBzdHJpbmc7XG4gICAgaXNUb2tlblZhbGlkO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB0b2tlbkhhbmRsaW5nU2VydmljZTogVG9rZW5IYW5kbGluZ1NlcnZpY2UsIHByaXZhdGUgand0SGVscGVyOiBKd3RIZWxwZXJTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgZGVjb2RlZFRva2VuU2VydmljZTogRGVjb2RlZFRva2VuU2VydmljZSkge1xuICAgICAgICB0aGlzLmp3dFRva2VuID0gbG9jYXRpb24uaHJlZi5zcGxpdCgnYWNjZXNzX3Rva2VuPScpWzFdO1xuICAgIH1cblxuICAgIGNhbkFjdGl2YXRlKFxuICAgICAgICBuZXh0OiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxuICAgICAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbiB7XG4gICAgICAgIHRoaXMuaXNUb2tlblZhbGlkID0gdGhpcy50b2tlbkhhbmRsaW5nU2VydmljZS5pc1Rva2VuVmFsaWQodGhpcy5qd3RUb2tlbik7XG4gICAgICAgIGlmICgodGhpcy5qd3RUb2tlbiAmJiB0aGlzLmlzVG9rZW5WYWxpZCkgfHxcbiAgICAgICAgICAgICh0aGlzLmp3dEhlbHBlci50b2tlbkdldHRlcigpICYmIHRoaXMudG9rZW5IYW5kbGluZ1NlcnZpY2UuaXNUb2tlblZhbGlkKHRoaXMuand0SGVscGVyLnRva2VuR2V0dGVyKCkpKSB8fFxuICAgICAgICAgICAgbG9jYXRpb24uaHJlZi5pbmNsdWRlcygncHBtYXN0ZXInKSkge1xuICAgICAgICAgICAgaWYgKHRoaXMuand0VG9rZW4pIHtcbiAgICAgICAgICAgICAgICBjb25zdCBhdXRoVmFsdWVzOiBhbnkgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oJ2F1dGhWYWx1ZXMnKSk7XG4gICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShhdXRoVmFsdWVzLmp3dFRva2VuS2V5LCB0aGlzLmp3dFRva2VuKTtcbiAgICAgICAgICAgICAgICB0aGlzLmRlY29kZWRUb2tlblNlcnZpY2Uuc2V0RGVjb2RlZEp3dFRva2VuKHRoaXMuand0VG9rZW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5jbGVhcigpO1xuICAgICAgICAgICAgdGhpcy50b2tlbkhhbmRsaW5nU2VydmljZS5yZWRpcmVjdFVSSSgpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19