/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var AlertService = /** @class */ (function () {
    function AlertService(_http) {
        this._http = _http;
    }
    /**
     * @param {?} url
     * @param {?} options
     * @return {?}
     */
    AlertService.prototype.getAlertsData = /**
     * @param {?} url
     * @param {?} options
     * @return {?}
     */
    function (url, options) {
        return this._http.get(url, options);
    };
    /**
     * @return {?}
     */
    AlertService.prototype.getEventData = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    AlertService.prototype.getAlertCounts = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} statusUrl
     * @param {?} statusObj
     * @return {?}
     */
    AlertService.prototype.changeStatus = /**
     * @param {?} statusUrl
     * @param {?} statusObj
     * @return {?}
     */
    function (statusUrl, statusObj) {
        return this._http.post(statusUrl, statusObj);
    };
    /**
     * @return {?}
     */
    AlertService.prototype.changeSeverity = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?} noteUrl
     * @param {?} noteObj
     * @return {?}
     */
    AlertService.prototype.addNote = /**
     * @param {?} noteUrl
     * @param {?} noteObj
     * @return {?}
     */
    function (noteUrl, noteObj) {
        return this._http.post(noteUrl, noteObj);
    };
    /**
     * @param {?} baseUrl
     * @param {?} baseObj
     * @return {?}
     */
    AlertService.prototype.saveCustomerKnowledgeBase = /**
     * @param {?} baseUrl
     * @param {?} baseObj
     * @return {?}
     */
    function (baseUrl, baseObj) {
        return this._http.post(baseUrl, baseObj);
    };
    AlertService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AlertService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    return AlertService;
}());
export { AlertService };
if (false) {
    /** @type {?} */
    AlertService.prototype._http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2NvbW1vbi11aS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2FsZXJ0LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFDLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBRzlEO0lBRUksc0JBQW9CLEtBQWlCO1FBQWpCLFVBQUssR0FBTCxLQUFLLENBQVk7SUFDckMsQ0FBQzs7Ozs7O0lBRUQsb0NBQWE7Ozs7O0lBQWIsVUFBYyxHQUFHLEVBQUUsT0FBVztRQUMxQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUF1QixHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7OztJQUVELG1DQUFZOzs7SUFBWjtJQUVBLENBQUM7Ozs7SUFFRCxxQ0FBYzs7O0lBQWQ7SUFFQSxDQUFDOzs7Ozs7SUFFRCxtQ0FBWTs7Ozs7SUFBWixVQUFhLFNBQVMsRUFBRSxTQUFTO1FBQzdCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQXVCLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7O0lBRUQscUNBQWM7OztJQUFkO0lBRUEsQ0FBQzs7Ozs7O0lBRUQsOEJBQU87Ozs7O0lBQVAsVUFBUSxPQUFPLEVBQUUsT0FBTztRQUNwQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUF1QixPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDbkUsQ0FBQzs7Ozs7O0lBRUQsZ0RBQXlCOzs7OztJQUF6QixVQUEwQixPQUFPLEVBQUUsT0FBTztRQUN0QyxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUF1QixPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDbkUsQ0FBQzs7Z0JBL0JKLFVBQVU7Ozs7Z0JBSEgsVUFBVTs7SUFxQ2xCLG1CQUFDO0NBQUEsQUFsQ0QsSUFrQ0M7U0FqQ1ksWUFBWTs7O0lBQ1QsNkJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7SHR0cENsaWVudCwgSHR0cFJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgQWxlcnRTZXJ2aWNlIHtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9odHRwOiBIdHRwQ2xpZW50KSB7XG4gICAgfVxuXG4gICAgZ2V0QWxlcnRzRGF0YSh1cmwsIG9wdGlvbnM6IHt9KTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8T2JqZWN0Pj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQ8SHR0cFJlc3BvbnNlPE9iamVjdD4+KHVybCwgb3B0aW9ucyk7XG4gICAgfVxuXG4gICAgZ2V0RXZlbnREYXRhKCk6IHZvaWQge1xuXG4gICAgfVxuXG4gICAgZ2V0QWxlcnRDb3VudHMoKTogdm9pZCB7XG5cbiAgICB9XG5cbiAgICBjaGFuZ2VTdGF0dXMoc3RhdHVzVXJsLCBzdGF0dXNPYmopOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxPYmplY3Q+PiB7XG4gICAgICAgIHJldHVybiB0aGlzLl9odHRwLnBvc3Q8SHR0cFJlc3BvbnNlPE9iamVjdD4+KHN0YXR1c1VybCwgc3RhdHVzT2JqKTtcbiAgICB9XG5cbiAgICBjaGFuZ2VTZXZlcml0eSgpOiB2b2lkIHtcblxuICAgIH1cblxuICAgIGFkZE5vdGUobm90ZVVybCwgbm90ZU9iaik6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPE9iamVjdD4+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAucG9zdDxIdHRwUmVzcG9uc2U8T2JqZWN0Pj4obm90ZVVybCwgbm90ZU9iaik7XG4gICAgfVxuXG4gICAgc2F2ZUN1c3RvbWVyS25vd2xlZGdlQmFzZShiYXNlVXJsLCBiYXNlT2JqKTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8T2JqZWN0Pj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5wb3N0PEh0dHBSZXNwb25zZTxPYmplY3Q+PihiYXNlVXJsLCBiYXNlT2JqKTtcbiAgICB9XG5cblxufVxuIl19