/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var NoDataAvailableComponent = /** @class */ (function () {
    function NoDataAvailableComponent() {
    }
    /**
     * @return {?}
     */
    NoDataAvailableComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    NoDataAvailableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-no-data-available',
                    template: "<div class=\"mdc-layout-grid no-data-available\" *ngIf=\"totalCount > 0\">\n  <h1 [translate]=\"'application.app.common.labels.NO_DATA_AVAILABLE'\">No Data Available</h1>\n</div>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    NoDataAvailableComponent.ctorParameters = function () { return []; };
    NoDataAvailableComponent.propDecorators = {
        totalCount: [{ type: Input }]
    };
    return NoDataAvailableComponent;
}());
export { NoDataAvailableComponent };
if (false) {
    /** @type {?} */
    NoDataAvailableComponent.prototype.totalCount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm8tZGF0YS1hdmFpbGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vY29tbW9uLXVpLyIsInNvdXJjZXMiOlsibm8tZGF0YS1hdmFpbGFibGUvbm8tZGF0YS1hdmFpbGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBUyxNQUFNLGVBQWUsQ0FBQztBQUV2RDtJQU9FO0lBQWdCLENBQUM7Ozs7SUFFakIsMkNBQVE7OztJQUFSO0lBQ0EsQ0FBQzs7Z0JBVkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLGdNQUFpRDs7aUJBRWxEOzs7Ozs2QkFFRSxLQUFLOztJQUtSLCtCQUFDO0NBQUEsQUFYRCxJQVdDO1NBTlksd0JBQXdCOzs7SUFDbkMsOENBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0LCBPbkluaXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtbm8tZGF0YS1hdmFpbGFibGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vbm8tZGF0YS1hdmFpbGFibGUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9uby1kYXRhLWF2YWlsYWJsZS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIE5vRGF0YUF2YWlsYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIHRvdGFsQ291bnQ6IG51bWJlcjtcbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxufVxuIl19