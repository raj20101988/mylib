/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { TopologyTreeComponent } from '../topology-tree/topology-tree.component';
import { TopologytreeService } from '../services/topologytree.service';
import { MaterialComponentModule } from '../material-component/material-component.module';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
/**
 * this module provide the topology tree component, service etc.. as you will add later like Pipe, Filters etc...
 * exported into the CommonUILib Module.
 */
var TopologyTreeModule = /** @class */ (function () {
    function TopologyTreeModule() {
    }
    TopologyTreeModule.decorators = [
        { type: NgModule, args: [{
                    imports: [MaterialComponentModule, CommonModule, DragDropModule],
                    declarations: [TopologyTreeComponent],
                    exports: [TopologyTreeComponent],
                    providers: [TopologytreeService],
                },] }
    ];
    return TopologyTreeModule;
}());
export { TopologyTreeModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9wb2xvZ3ktdHJlZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9jb21tb24tdWkvIiwic291cmNlcyI6WyJtb2R1bGVzL3RvcG9sb2d5LXRyZWUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBQyxxQkFBcUIsRUFBQyxNQUFNLDBDQUEwQyxDQUFDO0FBQy9FLE9BQU8sRUFBQyxtQkFBbUIsRUFBQyxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBQyx1QkFBdUIsRUFBQyxNQUFNLGlEQUFpRCxDQUFDO0FBQ3hGLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sd0JBQXdCLENBQUM7Ozs7O0FBTXREO0lBQUE7SUFNa0MsQ0FBQzs7Z0JBTmxDLFFBQVEsU0FBQztvQkFDTixPQUFPLEVBQUUsQ0FBQyx1QkFBdUIsRUFBRSxZQUFZLEVBQUUsY0FBYyxDQUFDO29CQUNoRSxZQUFZLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztvQkFDckMsT0FBTyxFQUFFLENBQUMscUJBQXFCLENBQUM7b0JBQ2hDLFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO2lCQUNuQzs7SUFDaUMseUJBQUM7Q0FBQSxBQU5uQyxJQU1tQztTQUF0QixrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtUb3BvbG9neVRyZWVDb21wb25lbnR9IGZyb20gJy4uL3RvcG9sb2d5LXRyZWUvdG9wb2xvZ3ktdHJlZS5jb21wb25lbnQnO1xuaW1wb3J0IHtUb3BvbG9neXRyZWVTZXJ2aWNlfSBmcm9tICcuLi9zZXJ2aWNlcy90b3BvbG9neXRyZWUuc2VydmljZSc7XG5pbXBvcnQge01hdGVyaWFsQ29tcG9uZW50TW9kdWxlfSBmcm9tICcuLi9tYXRlcmlhbC1jb21wb25lbnQvbWF0ZXJpYWwtY29tcG9uZW50Lm1vZHVsZSc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7RHJhZ0Ryb3BNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xuXG4vKipcbiAqIHRoaXMgbW9kdWxlIHByb3ZpZGUgdGhlIHRvcG9sb2d5IHRyZWUgY29tcG9uZW50LCBzZXJ2aWNlIGV0Yy4uIGFzIHlvdSB3aWxsIGFkZCBsYXRlciBsaWtlIFBpcGUsIEZpbHRlcnMgZXRjLi4uXG4gKiBleHBvcnRlZCBpbnRvIHRoZSBDb21tb25VSUxpYiBNb2R1bGUuXG4gKi9cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW01hdGVyaWFsQ29tcG9uZW50TW9kdWxlLCBDb21tb25Nb2R1bGUsIERyYWdEcm9wTW9kdWxlXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtUb3BvbG9neVRyZWVDb21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtUb3BvbG9neVRyZWVDb21wb25lbnRdLFxuICAgIHByb3ZpZGVyczogW1RvcG9sb2d5dHJlZVNlcnZpY2VdLFxufSlcbmV4cG9ydCBjbGFzcyBUb3BvbG9neVRyZWVNb2R1bGUgeyB9XG4iXX0=