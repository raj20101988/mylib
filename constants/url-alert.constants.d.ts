export interface UrlAlertConstantsInterface {
    socketAlert: String;
    alertsPath: String;
    alertCountsPath: String;
    alertConfigurationPath: String;
    metricsPath: String;
    filter: String;
}
export declare const alert: UrlAlertConstantsInterface;
