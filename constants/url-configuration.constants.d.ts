export interface UrlConfigurationConstantsInterface {
    categories: string;
    configurations: string;
}
export declare const configuration: UrlConfigurationConstantsInterface;
