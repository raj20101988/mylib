export interface UrlAuthenticationInterface {
    login: string;
    refresh: string;
}
export declare const auth: UrlAuthenticationInterface;
