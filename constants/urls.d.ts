import { UrlAlertConstantsInterface } from './url-alert.constants';
import { UrlAuthenticationInterface } from './url-authentication.constants';
import { UrlUserConstantsInterface } from './url-user.constants';
import { UrlConfigurationConstantsInterface } from './url-configuration.constants';
import { UrlTopologyTreeConstantsInterface } from './url-topologytree.constants';
export interface URLInterface {
    alert: UrlAlertConstantsInterface;
    auth: UrlAuthenticationInterface;
    user: UrlUserConstantsInterface;
    config: UrlConfigurationConstantsInterface;
    topologyTree: UrlTopologyTreeConstantsInterface;
}
export declare const urls: URLInterface;
