import { OnInit } from '@angular/core';
import { MatSnackBar, MatDialog } from '@angular/material';
export declare class AppSwitcherComponent implements OnInit {
    private snackBar;
    dialog: MatDialog;
    currentAppCode: any;
    constructor(snackBar: MatSnackBar, dialog: MatDialog);
    ngOnInit(): void;
    openAppMenu(): void;
}
