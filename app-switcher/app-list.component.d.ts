import { OnInit } from '@angular/core';
import { AuthService } from '../services/Authentication/auth.service';
import { UserService } from '../services/user.service';
export declare class AppListComponent implements OnInit {
    private authService;
    private userService;
    currentAppCode: any;
    appSwitcher: any;
    constructor(authService: AuthService, userService: UserService);
    ngOnInit(): void;
    divideMenuItems(): void;
    setFavoriteApp(event: any, appCode: any): void;
    openApp(clientID: any): void;
    currentApp(clientID: any, tokenData: any): void;
}
