import { EventEmitter, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
export declare class FilterComponent implements OnInit {
    private translate;
    configOptions: Object;
    EMIT_FILTER: EventEmitter<any>;
    filterConfigOption: any;
    searchKey: string;
    index: number;
    options: Array<any>;
    constructor(translate: TranslateService);
    ngOnInit(): void;
    selectOption(params: boolean): void;
    selectDeselectAll(params: any): void;
    selectDeselectOtherOptions(): void;
    emitFilter(params: any): void;
    translateFilterOptions(option: any): string;
}
